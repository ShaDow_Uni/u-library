package shadowuni.plugin.library.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ILibrary extends UILibrary {
	
	@Override
	public String getPlayerKey(String name) {
		Player p = Bukkit.getPlayer(name);
		return p == null ? null : getPlayerKey(p);
	}
	
	@Override
	public String getPlayerKey(Player player) {
		return player.getName().toLowerCase();
	}
	
	@Override
	public Player getPlayerByPlayerKey(String playerKey) {
		return Bukkit.getPlayer(playerKey);
	}
	
}