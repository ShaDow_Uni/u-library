package shadowuni.plugin.library.api.lib.barapi.nms;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.Registry;
import com.comphenix.protocol.wrappers.WrappedDataWatcher.WrappedDataWatcherObject;

public class v1_9P extends FakeDragon {
	private int id;

	public v1_9P(String name, Location loc) {
		super(name, loc);
	}

	@Override
	public Object getSpawnPacket() {
		PacketContainer spawn = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.SPAWN_ENTITY_LIVING);
		spawn.getIntegers().write(0, id);
		spawn.getIntegers().write(1, (int) EntityType.ENDER_DRAGON.getTypeId());
		spawn.getDataWatcherModifier().write(0, (WrappedDataWatcher) getWatcher());
		return spawn;
	}

	@Override
	public Object getDestroyPacket() {
		PacketContainer destroy = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.ENTITY_DESTROY);
		destroy.getIntegerArrays().write(0,  new int[] { id });
		return destroy;
	}

	@Override
	public Object getMetaPacket(Object watcher) {
		return watcher;
	}

	@Override
	public Object getTeleportPacket(Location loc) {
		
		PacketContainer teleport = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.ENTITY_TELEPORT);
		teleport.getIntegers().write(0, id);
		StructureModifier<Double> doubles = teleport.getDoubles();
		doubles.write(0, loc.getX());
		doubles.write(1, -300D);
		doubles.write(2, loc.getZ());

		return teleport;
	}

	@Override
	public Object getWatcher() {
		
		WrappedDataWatcher watcher = new WrappedDataWatcher();
		watcher.setObject(new WrappedDataWatcherObject(0, Registry.get(Byte.class, false)), Byte.valueOf((byte) 0x20));
		watcher.setObject(new WrappedDataWatcherObject(2, Registry.get(String.class, false)), name);
		watcher.setObject(new WrappedDataWatcherObject(3, Registry.get(Boolean.class, false)), Boolean.TRUE);
		watcher.setObject(new WrappedDataWatcherObject(7, Registry.get(Float.class, false)), Float.valueOf(20F));
		watcher.setObject(new WrappedDataWatcherObject(14, Registry.get(Integer.class, false)), 881);
		return watcher;
	}
}
