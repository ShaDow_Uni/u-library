package shadowuni.plugin.library.api.manager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLClassLoader;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.UnknownDependencyException;
import org.bukkit.plugin.java.JavaPlugin;

import shadowuni.plugin.library.api.object.UnRegisterableListener;

public class PluginManager {
	
	public Plugin getPlugin(String name) {
		for(Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
			if(!plugin.getName().equalsIgnoreCase(name)) continue;
			return plugin;
		}
		return null;
	}
	
	public void enablePlugin(Plugin plugin) {
		Bukkit.getPluginManager().enablePlugin(plugin);
	}
	
	public void disablePlugin(Plugin plugin) {
		Bukkit.getPluginManager().disablePlugin(plugin);
	}
	
	public Plugin loadPlugin(File file) {
		try {
			return Bukkit.getPluginManager().loadPlugin(file);
		} catch (UnknownDependencyException | InvalidPluginException | InvalidDescriptionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * unLoadPlugin
	 * 
	 *  플러그인이 없을 경우 null plugin
	 *  오류 발생 시 failed
	 *  완료 시 success
	 */
	
	@SuppressWarnings("unchecked")
	public String unLoadPlugin(Plugin plugin) {
		String name = plugin.getName();
		org.bukkit.plugin.PluginManager pluginManager = Bukkit.getPluginManager();
		
		if(pluginManager.getPlugin(name) == null) return "null plugin";
		
		SimpleCommandMap commandMap = null;
		List<Plugin> plugins = null;
		Map<String, Plugin> names = null;
		Map<String, Command> commands = null;
		
		try {
			Field pluginsField = Bukkit.getPluginManager().getClass().getDeclaredField("plugins");
			pluginsField.setAccessible(true);
			plugins = (List<Plugin>) pluginsField.get(pluginManager);
			
			Field lookupNamesField = Bukkit.getPluginManager().getClass().getDeclaredField("lookupNames");
			lookupNamesField.setAccessible(true);
			names = (Map<String, Plugin>) lookupNamesField.get(pluginManager);
			
			Field commandMapField = Bukkit.getPluginManager().getClass().getDeclaredField("commandMap");
			commandMapField.setAccessible(true);
			commandMap = (SimpleCommandMap) commandMapField.get(pluginManager);
			
			Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
			knownCommandsField.setAccessible(true);
			commands = (Map<String, Command>) knownCommandsField.get(commandMap);
		} catch(Exception e) {
			e.printStackTrace();
			return "failed";
		}
		
		disablePlugin(plugin);
		
		plugins.remove(plugin);
		
		names.remove(name);
		
		if(commandMap != null) {
			for(Iterator<Map.Entry<String, Command>> it = commands.entrySet().iterator(); it.hasNext(); ) {
				Map.Entry<String, Command> entry = it.next();
				if(entry.getValue() instanceof PluginCommand) {
					PluginCommand c = (PluginCommand) entry.getValue();
					if(c.getPlugin() == plugin) {
						c.unregister(commandMap);
						it.remove();
					}
				}
			}
		}
		
		try {
			((URLClassLoader) plugin.getClass().getClassLoader()).close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "success";
	}
	
	public File getFile(JavaPlugin plugin) {
		try {
			Field field = JavaPlugin.class.getDeclaredField("file");
			field.setAccessible(true);
			return (File) field.get(plugin);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void deletePlugin(JavaPlugin plugin) {
		try {
			unLoadPlugin(plugin);
			getFile(plugin).delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int registerListeners(JavaPlugin plugin) {
		int i = 0;
		try {
			ZipInputStream jarStream = new ZipInputStream(new FileInputStream(getFile(plugin)));
			ZipEntry item = null;
			while((item = jarStream.getNextEntry()) != null) {
				if(item.isDirectory() || !item.getName().endsWith(".class")) continue;
				String className = item.getName().replaceAll("/", ".");
				Class<?> c = Class.forName(className.substring(0, className.length() - 6));
				try {
					Listener listener = (Listener) c.newInstance();
					if(listener instanceof UnRegisterableListener) continue;
					Bukkit.getPluginManager().registerEvents(listener, plugin);
					i++;
				} catch (Exception ex) { }
			}
			jarStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
	public int registerListeners(JavaPlugin plugin, String pack) {
		int i = 0;
		try {
			ZipInputStream jarStream = new ZipInputStream(new FileInputStream(getFile(plugin)));
			ZipEntry item = null;
			while((item = jarStream.getNextEntry()) != null) {
				if(item.isDirectory() || !item.getName().endsWith(".class")) continue;
				String className = item.getName().replaceAll("/", ".").substring(0, item.getName().length() - 6);
				if(!className.substring(0, className.lastIndexOf(".")).equals(pack)) continue;
				Class<?> c = Class.forName(className);
				try {
					Listener listener = (Listener) c.newInstance();
					if(listener instanceof UnRegisterableListener) continue;
					Bukkit.getPluginManager().registerEvents(listener, plugin);
					i++;
				} catch (Exception ex) { }
			}
			jarStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
	public void registerCommand(JavaPlugin plugin, CommandExecutor executor, String... commands) {
		PluginCommand command = plugin.getCommand(commands[0]);
		if(commands.length > 1) {
			List<String> aliases = new ArrayList<>();
			for(int i = 1; i < commands.length; i++) {
				aliases.add(commands[i]);
			}
			command.setAliases(aliases);
		}
		command.setExecutor(executor);
	}
	
	@SuppressWarnings("unchecked")
	public void unRegisterCommands(Plugin plugin) {
		String name = plugin.getName();
		org.bukkit.plugin.PluginManager pluginManager = Bukkit.getPluginManager();
		
		if(pluginManager.getPlugin(name) == null) return;
		
		SimpleCommandMap commandMap = null;
		Map<String, Command> commands = null;
		
		try {
			Field commandMapField = Bukkit.getPluginManager().getClass().getDeclaredField("commandMap");
			commandMapField.setAccessible(true);
			commandMap = (SimpleCommandMap) commandMapField.get(pluginManager);
			
			Field knownCommandsField = SimpleCommandMap.class.getDeclaredField("knownCommands");
			knownCommandsField.setAccessible(true);
			commands = (Map<String, Command>) knownCommandsField.get(commandMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		if(commandMap != null) {
			for(Iterator<Map.Entry<String, Command>> it = commands.entrySet().iterator(); it.hasNext(); ) {
				Map.Entry<String, Command> entry = it.next();
				if(entry.getValue() instanceof PluginCommand) {
					PluginCommand c = (PluginCommand) entry.getValue();
					if(c.getPlugin() == plugin) {
						c.unregister(commandMap);
						it.remove();
					}
				}
			}
		}
	}
	
	public void unRegisterListeners(Plugin plugin) {
		HandlerList.unregisterAll(plugin);
	}
	
	public String getMD5(JavaPlugin plugin) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(getFile(plugin)));
			DigestInputStream dis = new DigestInputStream(bis, md);
			
			while(dis.read() != -1);
			dis.close();
			bis.close();
			
			Formatter formatter = new Formatter();
			for(byte b : md.digest()) {
				formatter.format("%02x", b);
			}
			String fs = formatter.toString();
			formatter.close();
			
			return fs;
		} catch (IOException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}