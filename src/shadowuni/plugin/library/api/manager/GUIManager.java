package shadowuni.plugin.library.api.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.library.api.object.gui.UGUI;
import shadowuni.plugin.library.api.object.gui.UQuickBarGUI;

@Getter
public class GUIManager {
	
	private HashMap<String, UGUI> playerGUIs = new HashMap<>();
	
	private HashMap<String, UQuickBarGUI> quickBarGUIs = new HashMap<>();
	
	public void setPlayerGUI(String name, UGUI gui) {
		playerGUIs.put(name.toLowerCase(), gui);
	}
	
	public void setPlayerGUI(Player p, UGUI gui) {
		setPlayerGUI(p.getName(), gui);
	}
	
	public void removePlayerGUI(String name) {
		playerGUIs.remove(name.toLowerCase());
	}
	
	public void removePlayerGUI(Player p) {
		removePlayerGUI(p.getName());
	}
	
	public UGUI getPlayerGUI(String name) {
		return playerGUIs.get(name.toLowerCase());
	}
	
	public UGUI getPlayerGUI(Player p) {
		return getPlayerGUI(p.getName());
	}
	
	public boolean hasGUI(String name) {
		return playerGUIs.containsKey(name.toLowerCase());
	}
	
	public boolean hasGUI(Player p) {
		return hasGUI(p.getName());
	}
	
	public List<Player> getPlayers(UGUI gui) {
		List<Player> players = new ArrayList<>();
		
		for(String name : playerGUIs.keySet()) {
			UGUI g = getPlayerGUI(name);
			if(!gui.equals(g)) continue;
			players.add(Bukkit.getPlayer(name));
		}
		
		return players;
	}
	
	public void setQuickBarGUI(String name, UQuickBarGUI gui) {
		quickBarGUIs.put(name.toLowerCase(), gui);
	}
	
	public void setQuickBarGUI(Player p, UQuickBarGUI gui) {
		setQuickBarGUI(p.getName(), gui);
	}
	
	public void removeQuickBarGUI(String name) {
		quickBarGUIs.remove(name.toLowerCase());
	}
	
	public void removeQuickBarGUI(Player p) {
		removeQuickBarGUI(p.getName());
	}
	
	public void clearQuickBarGUI(Player p) {
		if(!hasQuickBarGUI(p)) return;
		removeQuickBarGUI(p);
		if(p == null) return;
		p.getInventory().clear();
	}
	
	public UQuickBarGUI getQuickBarGUI(String name) {
		return quickBarGUIs.get(name.toLowerCase());
	}
	
	public UQuickBarGUI getQuickBarGUI(Player p) {
		return getQuickBarGUI(p.getName());
	}
	
	public boolean hasQuickBarGUI(String name) {
		return quickBarGUIs.containsKey(name.toLowerCase());
	}
	
	public boolean hasQuickBarGUI(Player p) {
		return hasQuickBarGUI(p.getName());
	}
	
	public List<Player> getOnlinePlayers(UQuickBarGUI gui) {
		List<Player> players = new ArrayList<>();
		
		for(String name : quickBarGUIs.keySet()) {
			if(!gui.equals(getQuickBarGUI(name))) continue;
			Player player = Bukkit.getPlayer(name);
			if(player == null) continue;
			players.add(player);
		}
		
		return players;
	}
	
	public List<String> getPlayers(UQuickBarGUI gui) {
		List<String> players = new ArrayList<>();
		
		for(String name : quickBarGUIs.keySet()) {
			if(!gui.equals(getQuickBarGUI(name))) continue;
			players.add(name);
		}
		
		return players;
	}
	
} 