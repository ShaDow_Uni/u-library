package shadowuni.plugin.library.api.manager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.entity.Player;

import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;
import shadowuni.plugin.library.api.category.NMSVersion;

public class ReflectionManager {
	
	private LibraryAPI api = LibraryPlugin.getApi();
	
	public Method getMethod(Class<?> clazz, String name) {
		for(Method method : clazz.getMethods()) {
			if(method.getName().equals(name)) return method;
		}
		return null;
	}
	
	public Class<?> getNMSClass(String name) {
		try { return Class.forName("net.minecraft.server." + api.getNMSVersion().getString() + "." + name); }
		catch (ClassNotFoundException e) { e.printStackTrace(); }
		return null;
	}
	
	public Object getHandle(Player p) {
		try { return getMethod(p.getClass(), "getHandle").invoke(p, null); }
		catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) { e.printStackTrace(); }
		return null;
	}
	
	public Object getPlayerConnection(Player p) {
		Object handle = getHandle(p);
		try { return handle.getClass().getField("playerConnection").get(handle); }
		catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) { e.printStackTrace(); }
		return null;
	}
	
	public Object getNetworkManager(Player p) {
		Object playerConnection = getPlayerConnection(p);
		try { return playerConnection.getClass().getField("networkManager").get(playerConnection); }
		catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) { e.printStackTrace(); }
		return null;
	}
	
	public void sendPacket(Player p, Object packet) {
		Object playerConnection = getPlayerConnection(p);
		try { getMethod(playerConnection.getClass(), "sendPacket").invoke(playerConnection, packet); } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) { e.printStackTrace(); }
	}
	
	public int getProtocolVersion(Player p) {
		Object networkManager = getNetworkManager(p);
		try {
			if(api.getNMSVersion().isLowerThan(NMSVersion.v1_8_R1)) {
				return Integer.valueOf(networkManager.getClass().getMethod("getVersion").invoke(networkManager, null).toString());
			} else {
				return 10000;
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException | NoSuchMethodException e) { e.printStackTrace(); }
		return 10000;
	}
	
}