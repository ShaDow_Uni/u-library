
package shadowuni.plugin.library.api.manager;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.jline.internal.InputStreamReader;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;

public class UpdateManager {
	
	private LibraryAPI lib = LibraryPlugin.getApi();
	
	@Getter
	private HashMap<String, String> lastVersion = new HashMap<>(); // <PluginName, Version>
	
	@Getter
	private HashMap<String, String> updateURL = new HashMap<>(); // <PluginName, URL>
	
	@Getter
	private HashMap<String, String> revision = new HashMap<>();
	
	public void refreshInfo() {
		lib.log("업데이트 정보를 불러오는 중입니다.");
		try {
			URL url = new URL("http://shadowuni.kr/update/update.json");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null) {
				sb.append(line);
			}
			JSONObject json = new JSONObject();
			JSONParser parser = new JSONParser();
			json = (JSONObject) parser.parse(sb.toString());
			for(Object k : json.keySet()) {
				String key = (String) k;
				JSONObject infoObj = (JSONObject) json.get(key);
				lastVersion.put(key, (String) infoObj.get("version"));
				updateURL.put(key, (String) infoObj.get("url"));
				revision.put(key, (String) infoObj.get("revision"));
			}
			reader.close();
		} catch(Exception e) {
			e.printStackTrace();
			if(e.getMessage().startsWith("Permission denied")) {
				Bukkit.shutdown();
				lib.log("정보를 불러올 수 없어 서버가 종료됩니다.");
			}
			return;
		}
		lib.log("업데이트 정보를 불러왔습니다.");
	}
	
	public boolean hasUpdate(JavaPlugin plugin) {
		return lastVersion.containsKey(plugin.getName()) && !lastVersion.get(plugin.getName()).equals(plugin.getDescription().getVersion());
	}
	
	public String updatePlugin(JavaPlugin plugin, boolean message, boolean reboot) {
		boolean r = revisionPlugin(plugin);
		if(!hasUpdate(plugin)) {
			if(message) { lib.log("최신 버전입니다."); }
			if(!r) return "no update";
		}
		if(message) {
			lib.log(r ? "플러그인 변조가 감지되어 다시 다운로드합니다." : "새로운 버전이 발견되어 업데이트를 진행합니다. (v" + lastVersion.get(plugin.getName()) + ")");
		}
		try {
			File file = lib.getPluginManager().getFile(plugin);
			URL url = new URL(updateURL.get(plugin.getName()));
			BufferedInputStream bis = new BufferedInputStream(url.openStream());
			FileOutputStream fos = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			long downloaded = 0;
			int read, count = 0, length = url.openConnection().getContentLength();
			while((read = bis.read(buf)) != -1) {
				fos.write(buf, 0, read);
				downloaded += read;
				count++;
				if(count < 3) continue;
				count = 0;
				lib.log("다운로드 중.. " + (downloaded * 100 / length) + "%");
			}
			fos.close();
			bis.close();
		} catch(Exception e) {
			e.printStackTrace();
			if(message) { lib.log("다운로드 중 오류가 발생했습니다."); }
			return "failed";
		}
		if(message) { lib.log("다운로드가 완료되었습니다."); }
		if(reboot) { lib.log("플러그인 적용을 위해 서버를 재부팅합니다."); Bukkit.shutdown(); }
		return "success";
	}
	
	public boolean revisionPlugin(JavaPlugin plugin) {
		String h = revision.get(plugin.getName());
		
		if(h != null && !h.equals(lib.getPluginManager().getMD5(plugin))) return true;
		
		return false;
	}
	
}