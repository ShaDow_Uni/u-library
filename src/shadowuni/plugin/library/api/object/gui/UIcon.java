package shadowuni.plugin.library.api.object.gui;

import java.util.HashMap;

import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;

@Getter
public abstract class UIcon {
	
	private final String key;
	
	@Setter
	private ItemStack item;
	
	private HashMap<String, Object> ETCs = new HashMap<>();
	
	public UIcon() {
		key = null;
	}
	
	public UIcon(String key) {
		this.key = key;
	}
	
	public void update() {
		item = updateItem();
	}
	
	public abstract ItemStack updateItem();
	
	public void setETC(String key, Object value) {
		ETCs.put(key, value);
	}
	
	public boolean existsETC(String key) {
		return ETCs.containsKey(key);
	}
	
	public Object getETC(String key) {
		return ETCs.get(key);
	}
	
}