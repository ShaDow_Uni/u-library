package shadowuni.plugin.library.api.object.gui;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.LibraryPlugin;

@Getter
public class UGUI {
	
	private final String title, key;
	
	private final int row;
	
	private Inventory inventory;
	
	@Setter
	private HashMap<Integer, UIcon> icons = new HashMap<>();
	
	@Setter
	private HashMap<String, Object> ETCs = new HashMap<>();
	
	public UGUI(String key, Inventory inventory) {
		this.key = key;
		this.inventory = inventory;
		title = inventory.getTitle();
		row = inventory.getSize() / 9;
	}
	
	public UGUI(String key, String title, int row) {
		this.key = key;
		this.title = title;
		this.row = row;
		inventory = Bukkit.createInventory(null, row * 9, ChatColor.translateAlternateColorCodes('&', title));
	}
	
	public void updateGUI() {};
	
	public void setETC(String key, Object value) {
		ETCs.put(key, value);
	}
	
	public boolean existsETC(String key) {
		return ETCs.containsKey(key);
	}
	
	public Object getETC(String key) {
		return ETCs.get(key);
	}
	
	public void setItem(int x, int y, ItemStack item) {
		inventory.setItem(x - 1 + (y - 1) * 9, item);
	}
	
	public void setItem(int pos, ItemStack item) {
		inventory.setItem(pos, item);
	}
	
	public ItemStack getItem(int x, int y) {
		return inventory.getItem(x - 1 + (y - 1) * 9);
	}
	
	public ItemStack getItem(int pos) {
		return inventory.getItem(pos);
	}
	
	public void setIcon(int x, int y, UIcon icon) {
		setIcon(x - 1 + (y - 1) * 9, icon);
	}
	
	public void setIcon(int pos, UIcon icon) {
		if(pos >= inventory.getSize()) throw new ArrayIndexOutOfBoundsException("인벤토리 밖에는 아이콘을 설정할 수 없습니다. (pos:" + pos + ")");
		icons.put(pos, icon);
	}
	
	public UIcon getIcon(int x, int y) {
		return icons.get(x - 1 + (y - 1) * 9);
	}
	
	public UIcon getIcon(int pos) {
		return icons.get(pos);
	}
	
	public void update() {
		updateGUI();
		updateIcons();
	}
	
	public void updateAsynchronously() {
		Bukkit.getScheduler().runTaskAsynchronously(LibraryPlugin.getInstance(), () -> update());
	}
	
	public void updateIcon(int x, int y) {
		updateIcon(x - 1 + (y - 1) * 9);
	}
	
	public void updateIcon(int pos) {
		UIcon icon = getIcon(pos);
		if(icon == null) return;
		icon.update();
		setItem(pos, icon.getItem());
	}
	
	public void updateIcons() {
		for(int pos : icons.keySet()) {
			updateIcon(pos);
		}
	}
	
	public void updateIconsAsynchronously() {
		Bukkit.getScheduler().runTaskAsynchronously(LibraryPlugin.getInstance(), () -> updateIcons());
	}
	
	public void open(Player p) {
		p.closeInventory();
		LibraryPlugin.getApi().getGUIManager().setPlayerGUI(p, this);
		Bukkit.getScheduler().runTask(LibraryPlugin.getInstance(), () -> p.openInventory(inventory));
	}
	
}