package shadowuni.plugin.library.api.object.gui;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;

@Getter
public class UQuickBarGUI {
	
	private HashMap<Integer, ItemStack> items = new HashMap<>();
	
	private HashMap<Integer, UIcon> icons = new HashMap<>();
	
	public void setItem(int x, ItemStack item) {
		items.put(x, item);
	}
	
	public ItemStack getItem(int x) {
		return items.get(x);
	}
	
	public void setIcon(int x, UIcon icon) {
		icons.put(x, icon);
	}
	
	public UIcon getIcon(int x) {
		return icons.get(x);
	}
	
	public void updateIcon(int x) {
		UIcon icon = getIcon(x);
		if(icon == null) return;
		icon.update();
		setItem(x, icon.getItem());
	}
	
	public void updateGUI() {
		for(int x : icons.keySet()) {
			updateIcon(x);
		}
	}
	
	public void setTo(Player p) {
		LibraryPlugin.getApi().getGUIManager().setQuickBarGUI(p, this);
		
		p.getInventory().clear();
		
		for(int i = 0; i < 9; i++) {
			p.getInventory().setItem(i, items.get(i + 1));
		}
	}
	
}