package shadowuni.plugin.library.api.object.command;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;
import shadowuni.plugin.library.api.object.UPlugin;

public abstract class USimpleCommandBase implements CommandExecutor {
	
	protected LibraryAPI lib = LibraryPlugin.getApi();
	
	@Getter
	private UPlugin plugin;
	
	@Getter
	private HashMap<String ,String> helpMessages = new LinkedHashMap<>(); // <Command, Comment>
	@Getter
	private HashMap<String, String> permMessages = new HashMap<>(); // <Command, Permission>
	
	public USimpleCommandBase(UPlugin plugin) {
		this.plugin = plugin;
	}
	
	protected boolean isConsole(CommandSender sender) {
		return !(sender instanceof Player);
	}
	
	protected boolean hasPermission(CommandSender sender, String perm, boolean message) {
		if(sender.hasPermission(perm) || sender.isOp()) return true;
		else if(message) { lib.msg(sender, "권한이 없습니다."); } return false;
	}
	
	protected void registerHelpMessage(String command, String comment) {
		helpMessages.put(command, comment);
	}
	
	protected void registerHelpMessage(String command, String comment, String perm) {
		registerHelpMessage(command, comment);
		permMessages.put(command, perm);
	}
	
	protected void sendHelpMessage(CommandSender sender, String label, boolean pluginMsg) {
		ChatColor color = lib.getPluginColor(plugin.getClass().getPackage().getName());
		if(pluginMsg) { lib.msg(sender, color + "v" + ChatColor.WHITE + plugin.getVersion()); }
		for(String command : helpMessages.keySet()) {
			if(permMessages.containsKey(command) && !sender.hasPermission(permMessages.get(command))) continue;
			lib.nmsg(sender, ChatColor.WHITE + "/" + command.replace("<command>", label) + color + " - " + helpMessages.get(command));
		}
	}
	
	protected void sendHelpMessage(CommandSender sender, String label, String command) {
		if(!helpMessages.containsKey(command)) return;
		lib.nmsg(sender, ChatColor.WHITE + "/" + command.replace("<command>", label) + lib.getPluginColor(plugin.getPluginPackage()) + " - " + helpMessages.get(command));
	}
	
	protected void sendHelpMessage(CommandSender sender, String label, int equal, String... args) {
		for(String command : helpMessages.keySet()) {
			String[] cs = command.split(" ");
			if(cs.length < 2) continue;
			int e = 0;
			for (int i =1; i < cs.length; i++) {
				if(args.length < i) break;
				if(!cs[i].equalsIgnoreCase(args[i - 1])) continue;
				e++;
				if(e != equal) continue;
				lib.msg(sender, ChatColor.WHITE + "/" + command.replace("<command>", label) + lib.getPluginColor(plugin.getPluginPackage()) + " - " + helpMessages.get(command));
				return;
			}
		}
	}
	
}