package shadowuni.plugin.library.api.object;

import java.io.IOException;
import java.net.URLClassLoader;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;

public abstract class UPlugin extends JavaPlugin {
	
	protected LibraryAPI lib = LibraryPlugin.getApi();
	
	@Getter
	protected String version;
	@Getter
	private String pluginPackage;
	
	@Setter
	@Getter
	private boolean deleteOnExit;
	
	public void onUEnable() {}
	public void onUDisable() {}
	
	public void onEnable() {
		onUEnable();
		version = getDescription().getVersion();
		lib.log(pluginPackage, "플러그인이 활성화되었습니다. (v" + version + ")");
	}
	
	public void onDisable() {
		onUDisable();
		if(isDeleteOnExit()) {
			try {
				((URLClassLoader) getClass().getClassLoader()).close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			getFile().delete();
		}
		lib.log(pluginPackage, "플러그인이 비활성화되었습니다. (v" + getDescription().getVersion() + ")");
	}
	
	public void disable() {
		lib.getPluginManager().disablePlugin(this);
	}
	
	protected void registerPrefix(String prefix) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerPrefix(pluginPackage, prefix);
	}
	
	protected void registerPluginColor(ChatColor color) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerColor(pluginPackage, color);
	}
	
	protected void registerListeners() {
		lib.getPluginManager().registerListeners(this);
	}
	
	protected void registerListeners(String pack) {
		lib.getPluginManager().registerListeners(this, pack);
	}
	
	protected void registerCommand(CommandExecutor executor, String... commands) {
		lib.getPluginManager().registerCommand(this, executor, commands);
	}
	
	protected String getMD5() {
		return lib.getPluginManager().getMD5(this);
	}
	
	protected boolean updatePlugin() {
		return lib.getUpdateManager().updatePlugin(this, true, true).equals("success");
	}
	
}