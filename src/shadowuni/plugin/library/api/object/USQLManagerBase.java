package shadowuni.plugin.library.api.object;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;

public class USQLManagerBase {
	
	protected LibraryAPI lib = LibraryPlugin.getApi();
	
	@Setter
	@Getter
	private String SQLAddress, SQLDatabase, SQLUser, SQLPassword, SQLTablePrefix;
	
	@Setter
	@Getter
	private int SQLPort;
	
	@Setter
	@Getter
	private boolean use, useUseOption;
	
	@Getter
	private FileConfiguration config;
	
	protected Connection conn = null;
	
	public void loadSQLConfig(JavaPlugin plugin) {
		createSQLConfig(plugin);
		config = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "mysql-config.yml"));
		if(useUseOption) {
			use = config.getBoolean("사용");
		}
		SQLAddress = config.getString("주소");
		SQLPort = config.getInt("포트");
		SQLDatabase = config.getString("데이터베이스");
		SQLUser = config.getString("유저");
		SQLPassword = config.getString("비밀번호");
		SQLTablePrefix = config.getString("테이블 접두사");
		loadSQLConfigOthers();
		lib.log("MySQL 설정을 불러왔습니다.");
	}
	
	private void createSQLConfig(JavaPlugin plugin) {
		try {
			File file = new File(plugin.getDataFolder(), "mysql-config.yml");
			if(file.exists()) return;
			YamlConfiguration config = new YamlConfiguration();
			if(useUseOption) {
				config.set("사용", false);
			}
			config.set("주소", "localhost");
			config.set("포트", 3306);
			config.set("데이터베이스", "database");
			config.set("유저", "root");
			config.set("비밀번호", "password");
			config.set("테이블 접두사", "u_");
			createSQLConfigOthers();
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadSQLConfigOthers() { }
	public void createSQLConfigOthers() { }
	
	public boolean connect(JavaPlugin plugin) {
		try {
			loadSQLConfig(plugin);
			if(useUseOption && !use) return false;
			conn = DriverManager.getConnection("jdbc:mysql://" + SQLAddress + ":" + SQLPort + "/" + SQLDatabase + "?autoReconnect=true", SQLUser, SQLPassword);
			createTable();
			lib.log("MySQL에 접속되었습니다.");
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			lib.log("MySQL에 연결할 수 없습니다!");
			return false;
		}
	}
	
	public void close() {
		try {
			if(conn == null) return;
			conn.close();
			lib.log("MySQL과의 연결을 종료했습니다.");
		} catch(Exception e) {
			lib.log("MySQL과의 연결을 종료하는 중 오류가 발생했습니다!");
		}
	}
	
	public boolean isConnected() {
		return conn != null;
	}
	
	public void update(String sql) {
		try {
			PreparedStatement state = conn.prepareStatement(sql);
			state.executeUpdate();
			state.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createTable() { }
	
	public void createTable(String name, String calumn) {
		update("create table if not exists " + SQLTablePrefix + name + " (" + calumn + ")");
	}
	
}