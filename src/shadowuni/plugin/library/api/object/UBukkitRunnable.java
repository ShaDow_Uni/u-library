package shadowuni.plugin.library.api.object;


import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.LibraryAPI;

public abstract class UBukkitRunnable implements Runnable {
	
	protected LibraryAPI lib = LibraryPlugin.getApi();
	
	private int taskId = -1;

	public synchronized void cancel() throws IllegalStateException {
		Bukkit.getScheduler().cancelTask(getTaskId());
		taskId = -1;
	}

	public synchronized BukkitTask runTask(Plugin plugin) throws IllegalArgumentException, IllegalStateException {
		checkState();
		return setupId(Bukkit.getScheduler().runTask(plugin, this));
	}

	public synchronized BukkitTask runTaskAsynchronously(Plugin plugin)
			throws IllegalArgumentException, IllegalStateException {
		checkState();
		return setupId(Bukkit.getScheduler().runTaskAsynchronously(plugin, this));
	}

	public synchronized BukkitTask runTaskLater(Plugin plugin, long delay)
			throws IllegalArgumentException, IllegalStateException {
		checkState();
		return setupId(Bukkit.getScheduler().runTaskLater(plugin, this, delay));
	}

	public synchronized BukkitTask runTaskLaterAsynchronously(Plugin plugin, long delay)
			throws IllegalArgumentException, IllegalStateException {
		checkState();
		return setupId(Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, this, delay));
	}

	public synchronized BukkitTask runTaskTimer(Plugin plugin, long delay, long period)
			throws IllegalArgumentException, IllegalStateException {
		checkState();
		return setupId(Bukkit.getScheduler().runTaskTimer(plugin, this, delay, period));
	}
	
	public synchronized BukkitTask runTaskTimerAsynchronously(Plugin plugin, long delay, long period)
			throws IllegalArgumentException, IllegalStateException {
		checkState();
		return setupId(Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, this, delay, period));
	}
	
	public BukkitTask schedule(Plugin plugin, Date time) {
		checkState();
		return setupId(runTaskLater(plugin, (long) Math.ceil((time.getTime() - System.currentTimeMillis()) / 50)));
	}
	
	public BukkitTask schedule(Plugin plugin, Date firstTime, long period) {
		checkState();
		return setupId(runTaskTimer(plugin, (long) Math.ceil((firstTime.getTime() - System.currentTimeMillis()) / 50), period));
	}
	
	public BukkitTask scheduleAsynchronously(Plugin plugin, Date time) {
		checkState();
		return setupId(runTaskLaterAsynchronously(plugin, (long) Math.ceil((time.getTime() - System.currentTimeMillis()) / 50)));
	}
	
	public BukkitTask scheduleAsynchronously(Plugin plugin, Date firstTime, long period) {
		checkState();
		return setupId(runTaskTimerAsynchronously(plugin, (long) Math.ceil((firstTime.getTime() - System.currentTimeMillis()) / 50), period));
	}
	
	public synchronized int getTaskId() throws IllegalStateException {
		return this.taskId;
	}

	private void checkState() {
		if (this.taskId != -1)
			throw new IllegalStateException("Already scheduled as " + this.taskId);
	}

	private BukkitTask setupId(BukkitTask task) {
		this.taskId = task.getTaskId();
		return task;
	}
	
}