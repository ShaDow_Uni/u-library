package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.category.InventoryAction;
import shadowuni.plugin.library.api.object.gui.UGUI;
import shadowuni.plugin.library.api.object.gui.UIcon;

public class GUIClickEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	@Setter
	@Getter
	private boolean cancelled = false;
	
	@Getter
	private final Player player;
	
	@Getter
	private final InventoryAction action;
	
	@Getter
	private UGUI GUI;
	
	@Getter
	private InventoryClickEvent inventoryClickEvent;
	
	public GUIClickEvent(InventoryClickEvent inventoryClickEvent) {
		this.inventoryClickEvent = inventoryClickEvent;
		player = (Player) inventoryClickEvent.getWhoClicked();
		switch(inventoryClickEvent.getAction()) {
		case PICKUP_ALL:
			action = InventoryAction.LEFT_HOLD; break;
		case PLACE_ALL:
			action = InventoryAction.LEFT_PLACE; break;
		case PICKUP_HALF:
			action = InventoryAction.RIGHT_HOLD; break;
		case PLACE_ONE:
			action = InventoryAction.RIGHT_PLACE; break;
		case MOVE_TO_OTHER_INVENTORY:
			action = InventoryAction.SHIFT_CLICK; break;
		case COLLECT_TO_CURSOR:
			action = InventoryAction.DOUBLE_CLICK; break;
		case CLONE_STACK:
			action = InventoryAction.WHEEL; break;
		case DROP_ONE_SLOT:
			action = InventoryAction.DROP; break;
		case NOTHING:
			action = InventoryAction.NOTHING; break;
		default:
			action = InventoryAction.OTHER; break;
		}
		GUI = LibraryPlugin.getApi().getGUIManager().getPlayerGUI(player);
	}
	
	public int getClickedX() {
		return inventoryClickEvent.getSlot() % 9 + 1;
	}
	
	public int getClickedY() {
		return inventoryClickEvent.getSlot() / 9 + 1;
	}
	
	public int getClickedSlot() {
		return inventoryClickEvent.getSlot();
	}
	
	public ItemStack getClickedItem() {
		return GUI.getInventory().getItem(getClickedSlot());
	}
	
	public UIcon getClickedIcon() {
		return GUI.getIcon(getClickedSlot());
	}
	
	public boolean isIconClicked() {
		return getClickedIcon() != null;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}