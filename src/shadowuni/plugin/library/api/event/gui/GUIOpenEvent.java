package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryOpenEvent;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.object.gui.UGUI;

public class GUIOpenEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	@Getter
	private final Player player;
	
	@Getter
	private final InventoryOpenEvent inventoryOpenEvent;
	
	@Getter
	private UGUI GUI;
	
	public GUIOpenEvent(InventoryOpenEvent inventoryOpenEvent) {
		this.inventoryOpenEvent = inventoryOpenEvent;
		player = (Player) inventoryOpenEvent.getPlayer();
		GUI = LibraryPlugin.getApi().getGUIManager().getPlayerGUI(player);
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}