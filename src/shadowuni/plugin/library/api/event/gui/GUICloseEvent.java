package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryCloseEvent;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.object.gui.UGUI;

public class GUICloseEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	@Getter
	private final Player player;
	
	@Getter
	private final InventoryCloseEvent inventoryCloseEvent;
	
	@Getter
	private UGUI GUI;
	
	public GUICloseEvent(InventoryCloseEvent inventoryCloseEvent) {
		this.inventoryCloseEvent = inventoryCloseEvent;
		player = (Player) inventoryCloseEvent.getPlayer();
		GUI = LibraryPlugin.getApi().getGUIManager().getPlayerGUI(player);
	}

	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}
