package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.category.ClickAction;
import shadowuni.plugin.library.api.object.gui.UQuickBarGUI;

public class QuickBarGUIClickEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	@Getter
	private final Player player;
	
	@Getter
	private final ClickAction clickAction;
	
	@Getter
	private UQuickBarGUI GUI;
	
	@Getter
	private PlayerInteractEvent playerInteractEvent;
	
	public QuickBarGUIClickEvent(PlayerInteractEvent playerInteractEvent) {
		this.playerInteractEvent = playerInteractEvent;
		player = playerInteractEvent.getPlayer();
		clickAction = (playerInteractEvent.getAction() == Action.LEFT_CLICK_AIR || playerInteractEvent.getAction() == Action.LEFT_CLICK_BLOCK) ? ClickAction.LEFT_CLICK : ClickAction.RIGHT_CLICK;
		GUI = LibraryPlugin.getApi().getGUIManager().getQuickBarGUI(player);
	}
	
	public ItemStack getClickedItem() {
		return player.getItemInHand();
	}
	
	public int getClickedX() {
		return  player.getInventory().getHeldItemSlot() + 1;
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}