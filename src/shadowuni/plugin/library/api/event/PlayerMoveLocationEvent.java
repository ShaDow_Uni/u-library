package shadowuni.plugin.library.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerMoveEvent;

import lombok.Getter;
import lombok.Setter;

public class PlayerMoveLocationEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	@Setter
	@Getter
	private boolean cancelled = false;
	
	@Getter
	private final Player player;
	
	@Getter
	private PlayerMoveEvent playerMoveEvent;
	
	public PlayerMoveLocationEvent(PlayerMoveEvent playerMoveEvent) {
		this.playerMoveEvent = playerMoveEvent;
		this.player = playerMoveEvent.getPlayer();
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
}