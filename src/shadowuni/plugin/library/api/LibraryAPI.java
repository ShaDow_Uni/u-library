package shadowuni.plugin.library.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

import lombok.Getter;
import shadowuni.plugin.library.LibraryPlugin;
import shadowuni.plugin.library.api.category.NMSVersion;
import shadowuni.plugin.library.api.lib.VaultHandler;
import shadowuni.plugin.library.api.lib.barapi.BarAPI;
import shadowuni.plugin.library.api.manager.GUIManager;
import shadowuni.plugin.library.api.manager.ItemManager;
import shadowuni.plugin.library.api.manager.PluginManager;
import shadowuni.plugin.library.api.manager.ReflectionManager;
import shadowuni.plugin.library.api.manager.UpdateManager;

@Getter
public class LibraryAPI {
	
	private final String libPrefix = ChatColor.YELLOW + "[ U-Library ] " + ChatColor.WHITE;
	private NMSVersion NMSVersion;
	
	private HashMap<String, String> pluginPrefixes = new HashMap<>(); // <Package, PluginPrefix>
	private HashMap<String, ChatColor> pluginColors = new HashMap<>(); // <Package, Color>
	
	private BarAPI bar = new BarAPI();
	private VaultHandler vault = new VaultHandler();
	private PluginManager pluginManager;
	private UpdateManager updateManager;
	private ItemManager itemManager;
	private ReflectionManager reflectionManager;
	private GUIManager GUIManager;
	
	public void init() {
		if(Bukkit.getPluginManager().getPlugin("Vault") != null) {
			vault.setupChat();
			vault.setupPermission();
			vault.setupEconomy();
		}
		pluginManager = new PluginManager();
		updateManager = new UpdateManager();
		itemManager = new ItemManager();
		reflectionManager = new ReflectionManager();
		GUIManager = new GUIManager();
		String pg = Bukkit.getServer().getClass().getPackage().getName();
		NMSVersion = shadowuni.plugin.library.api.category.NMSVersion.getByName(pg.substring(pg.lastIndexOf(".") + 1));
	}
	
	public String getLastClassName() {
		StackTraceElement[] ste = new Throwable().getStackTrace();
		for(int i = 0; i < ste.length; i++) {
			if(!ste[i].getClassName().startsWith(LibraryPlugin.class.getPackage().getName())) return ste[i].getClassName();
		}
		return null;
	}
	
	public void log(String message) {
		log(getLastClassName(), message);
	}
	
	public void log(String pack, String message) {
		nlog(getPrefix(pack) + message);
	}
	
	public void nlog(String message) {
		Bukkit.getConsoleSender().sendMessage(message);
	}
	
	public void msg(CommandSender sender, String message) {
		msg(sender, getLastClassName(), message);
	}
	
	public void msg(CommandSender sender, String pack, String message) {
		nmsg(sender, getPrefix(pack) + message);
	}
	
	public void nmsg(CommandSender sender, String message) {
		sender.sendMessage(message);
	}
	
	public void broadcast(String message) {
		nbroadcast(getPrefix(getLastClassName()) + message);
	}
	
	public void nbroadcast(String message) {
		Bukkit.broadcastMessage(message);
	}
	
	public void registerPrefix(String startPackage, String prefix) {
		pluginPrefixes.put(startPackage, prefix);
	}
	
	public void registerColor(String startPackage, ChatColor color) {
		pluginColors.put(startPackage, color);
	}
	
	public String getPrefix(String classPackage) {
		if(classPackage == null) return libPrefix;
		for(String pack : pluginPrefixes.keySet()) {
			if(classPackage.equals(pack) || (classPackage.startsWith(pack) && classPackage.length() > pack.length() && classPackage.substring(pack.length(), pack.length() + 1).equals("."))) return pluginPrefixes.get(pack);
		}
		return libPrefix;
	}
	
	public ChatColor getPluginColor(String classPackage) {
		if(classPackage == null) return ChatColor.WHITE;
		for(String pack : pluginColors.keySet()) {
			if(classPackage.startsWith(pack)) return pluginColors.get(pack);
		}
		return ChatColor.WHITE;
	}
	
	public boolean isSpigot() {
		try { return Class.forName("org.bukkit.Server.Spigot") != null; }
		catch (ClassNotFoundException e) { }
		return false;
	}
	
	public List<Player> getOnlinePlayers() {
		List<Player> players = new ArrayList<>();
		try {
			Object result = reflectionManager.getMethod(Bukkit.class, "getOnlinePlayers").invoke(null, null);
			if(result instanceof Player[]) {
				players.addAll(Arrays.asList((Player[]) result));
			} else {
				players.addAll((Collection) result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return players;
	}
	
	public void teleport(Player p, Location location) {
		if(p.getVehicle() != null) {
			p.leaveVehicle();
		}
		p.teleport(location);
	}
	
	public Firework spawnFirework(boolean flicker, boolean trail, Type type, Color color, Color fade, int power, Location location) {
		Firework firework = location.getWorld().spawn(location, Firework.class);
		FireworkMeta meta = firework.getFireworkMeta();
		FireworkEffect fe = FireworkEffect.builder().flicker(flicker).trail(trail).with(type).withColor(color).withFade(fade).build();
		meta.setPower(power);
		meta.addEffect(fe);
		firework.setFireworkMeta(meta);
		return firework;
	}
	
}