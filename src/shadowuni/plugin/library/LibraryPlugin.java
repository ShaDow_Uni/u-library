package shadowuni.plugin.library;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.library.api.LibraryAPI;
import shadowuni.plugin.library.api.object.UPlugin;

public class LibraryPlugin extends UPlugin {
	
	@Getter
	private static LibraryPlugin instance;
	@Getter
	private static LibraryAPI api = new LibraryAPI();
	
	public void onUEnable() {
		instance = this;
		api.init();
		api.getUpdateManager().refreshInfo();
		if(updatePlugin()) return;
		registerListeners();
		pluginCheck();
	}
	
	public void onUDisable() {
		api.getBar().clearBar();
		
		for(String name : api.getGUIManager().getPlayerGUIs().keySet()) {
			Player p = Bukkit.getPlayer(name);
			if(p == null) return;
			p.closeInventory();
		}
	}
	
	public void pluginCheck() {
		lib.getBar().setUseProtocolSupport(Bukkit.getPluginManager().getPlugin("ProtocolSupport") != null);
	}
	
}