package shadowuni.plugin.library.listener;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import shadowuni.plugin.library.api.event.PlayerMoveLocationEvent;

public class PlayerListener implements Listener {
	
	private HashMap<String, Location> moveLogs = new HashMap<>();
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		
		Location old = moveLogs.get(p.getName().toLowerCase());
		Location nw = p.getLocation();
		
		if(old == null) { }
		else if(old.getX() == nw.getX() && old.getY() == nw.getY() && old.getZ() == nw.getZ()) return;
		
		moveLogs.put(p.getName().toLowerCase(), nw);
		
		PlayerMoveLocationEvent event = new PlayerMoveLocationEvent(e);
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled()) {
			e.setTo(e.getFrom());
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		moveLogs.remove(e.getPlayer().getName().toLowerCase());
	}
	
}