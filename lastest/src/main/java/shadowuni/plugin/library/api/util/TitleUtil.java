package shadowuni.plugin.library.api.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TitleUtil {
	
	public static void setTitle(Player p, int fadeIn, int fadeOut, int stay, String msg) {
		try {
			msg = ChatColor.translateAlternateColorCodes('&', msg);
			Object eTitle = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null);
			Object cTitle = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + msg + "\"}");
			Constructor<?> tc = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getConstructor(ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], ReflectionUtil.getNMSClass("IChatBaseComponent"), int.class, int.class, int.class);
			Object packet = tc.newInstance(new Object[] {eTitle, cTitle, fadeIn, stay, fadeOut});
			ReflectionUtil.sendPacket(p, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setSubTitle(Player p, int fadeIn, int fadeOut, int stay, String msg) {
		try {
			msg = ChatColor.translateAlternateColorCodes('&', msg);
			Object eTitle = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get(null);
			Object cTitle = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + msg + "\"}");
			Constructor<?> tc = ReflectionUtil.getNMSClass("PacketPlayOutTitle").getConstructor(ReflectionUtil.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], ReflectionUtil.getNMSClass("IChatBaseComponent"), int.class, int.class, int.class);
			Object packet = tc.newInstance(eTitle, cTitle, fadeIn, stay, fadeOut);
			ReflectionUtil.sendPacket(p, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setTabTitle(Player p, String header, String footer) {
		try {
			header = ChatColor.translateAlternateColorCodes('&', header);
			footer = ChatColor.translateAlternateColorCodes('&', footer);
			Object tHeader = ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + header + "\"}");
			Object tFooter= ReflectionUtil.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", String.class).invoke(null, "{\"text\":\"" + footer + "\"}");
			Constructor<?> tc = ReflectionUtil.getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor(ReflectionUtil.getNMSClass("IChatBaseComponent"));
			Object packet = tc.newInstance(tHeader);
			Field f = packet.getClass().getDeclaredField("b");
			f.setAccessible(true);
			f.set(packet, tFooter);
			ReflectionUtil.sendPacket(p, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setActionBar(Player p, String msg) {
		try {
			Object slotTitle = ReflectionUtil.getNMSClass("IChatBaseComponent$ChatSerializer").getMethod("a", String.class).invoke(null, "{\"text\":\"" + msg + "\"}");
			Constructor<?> tc = ReflectionUtil.getNMSClass("PacketPlayOutChat").getConstructor(ReflectionUtil.getNMSClass("IChatBaseComponent"), byte.class);
			Object packet = tc.newInstance(slotTitle, (byte) 2);
			ReflectionUtil.sendPacket(p, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}