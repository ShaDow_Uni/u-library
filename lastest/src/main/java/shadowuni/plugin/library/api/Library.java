package shadowuni.plugin.library.api;

import java.util.UUID;

import org.bukkit.entity.Player;

public class Library extends ULibrary {
	
	public Library(UPlugin plugin) {
		UILibrary = new ILibrary();
		libraryPlugin = plugin;
	}
	
	public static Player getPlayerByUUID(UUID UUID) {
		for(Player p : getOnlinePlayers()) {
			if(p.getUniqueId().equals(UUID)) return p;
		}
		
		return null;
	}
	
}