package shadowuni.plugin.library;


import lombok.Getter;
import shadowuni.plugin.library.api.ILibraryPlugin;
import shadowuni.plugin.library.api.Library;

public class LibraryPlugin extends ILibraryPlugin {
	
	@Getter
	private static Library api;
	
	public void onLoad() {
		api = new Library(this);
	}
	
}