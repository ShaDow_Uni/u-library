package shadowuni.plugin.library.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.event.ChannelMessageEvent;

public class ChannelMessageListener implements PluginMessageListener {
	
	private ULibrary lib = ULibrary.getInstance();
	
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if(!channel.equals("U-BLibrary")) return;
		
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		
		String key = in.readUTF();
		String task = in.readUTF();
		
		Bukkit.getPluginManager().callEvent(new ChannelMessageEvent(key, task, in));
	}
	
}