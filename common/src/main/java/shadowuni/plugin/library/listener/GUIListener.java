package shadowuni.plugin.library.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.event.gui.GUIClickEvent;
import shadowuni.plugin.library.api.event.gui.GUICloseEvent;
import shadowuni.plugin.library.api.event.gui.QuickBarGUIClickEvent;
import shadowuni.plugin.library.api.gui.GUI;

public class GUIListener implements Listener {
	
	private ULibrary api = ULibrary.getInstance();
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getRawSlot() < 0 || e.getRawSlot() > e.getInventory().getSize()) return;
		
		Player p = (Player) e.getWhoClicked();
		
		if(api.getGUIManager().hasGUI(p.getName())) {
			GUI gui = api.getGUIManager().getPlayerGUI(p);
			
			GUIClickEvent event = new GUIClickEvent(e);
			Bukkit.getPluginManager().callEvent(event);
			e.setCancelled(event.isCancelled());
		} else if(e.getSlotType() == SlotType.QUICKBAR && api.getGUIManager().hasQuickBarGUI(p.getName())) {
			e.setCancelled(true);
			p.updateInventory();
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		
		if(!api.getGUIManager().hasGUI(p.getName())) return;
		
		GUICloseEvent event = new GUICloseEvent(e);
		Bukkit.getPluginManager().callEvent(event);
		
		api.getGUIManager().removePlayerGUI(p);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getAction() == Action.PHYSICAL || !api.getGUIManager().hasQuickBarGUI(e.getPlayer())) return;
		
		QuickBarGUIClickEvent event = new QuickBarGUIClickEvent(e);
		Bukkit.getPluginManager().callEvent(event);
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		if(!api.getGUIManager().hasQuickBarGUI(e.getPlayer())) return;
		
		api.getGUIManager().getQuickBarGUI(e.getPlayer()).setTo(e.getPlayer());
		e.getItemDrop().remove();
	}
	
	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent e) {
		if(!api.getGUIManager().hasQuickBarGUI(e.getPlayer())) return;
		
		e.setCancelled(true);
	}
	
}