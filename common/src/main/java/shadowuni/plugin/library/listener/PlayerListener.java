package shadowuni.plugin.library.listener;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import shadowuni.plugin.library.api.ILibraryPlugin;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.event.FirstPlayerJoinEvent;
import shadowuni.plugin.library.api.event.LastPlayerQuitEvent;
import shadowuni.plugin.library.api.event.PlayerMoveLocationEvent;

public class PlayerListener implements Listener {
	
	private HashMap<String, Location> moveLogs = new HashMap<>();
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent e) {
		if(ULibrary.getSQLManager().isConnected() && ULibrary.getSQLManager().isUseUpload()) {
			Bukkit.getScheduler().runTaskAsynchronously(ILibraryPlugin.getInstance(), () -> ULibrary.getSQLManager().savePlayerKey(e.getPlayer().getName(), ULibrary.getPlayerKey(e.getPlayer())));
		}
		
		if(ULibrary.getOnlinePlayers().size() > 1) return;
		
		Bukkit.getPluginManager().callEvent(new FirstPlayerJoinEvent(e.getPlayer(), e));
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		
		Location old = moveLogs.get(p.getName().toLowerCase());
		Location nw = p.getLocation();
		
		if(old != null && old.distance(nw) < 0.1) return;
		
		moveLogs.put(p.getName().toLowerCase(), nw);
		
		PlayerMoveLocationEvent event = new PlayerMoveLocationEvent(p, e);
		Bukkit.getPluginManager().callEvent(event);
		if(event.isCancelled()) {
			e.setTo(e.getFrom());
		}
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent e) {
		moveLogs.remove(e.getPlayer().getName().toLowerCase());
		
		if(ULibrary.getOnlinePlayers().size() > 1) return;
		
		Bukkit.getPluginManager().callEvent(new LastPlayerQuitEvent(e.getPlayer(), e));
	}
	
}