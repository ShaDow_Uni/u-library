package shadowuni.plugin.library.api.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;

public class GUIManager {
	
	@Getter
	private HashMap<String, GUI> playerGUIs = new HashMap<>();
	
	@Getter
	private HashMap<String, QuickBarGUI> quickBarGUIs = new HashMap<>();
	
	public void setPlayerGUI(String name, GUI gui) {
		playerGUIs.put(name.toLowerCase(), gui);
	}
	
	public void setPlayerGUI(Player p, GUI gui) {
		setPlayerGUI(p.getName(), gui);
	}
	
	public void removePlayerGUI(String name) {
		playerGUIs.remove(name.toLowerCase());
	}
	
	public void removePlayerGUI(Player p) {
		removePlayerGUI(p.getName());
	}
	
	public GUI getPlayerGUI(String name) {
		return playerGUIs.get(name.toLowerCase());
	}
	
	public GUI getPlayerGUI(Player p) {
		return getPlayerGUI(p.getName());
	}
	
	public boolean hasGUI(String name) {
		return playerGUIs.containsKey(name.toLowerCase());
	}
	
	public boolean hasGUI(Player p) {
		return hasGUI(p.getName());
	}
	
	public List<Player> getPlayers(GUI gui) {
		List<Player> players = new ArrayList<>();
		
		for(String name : playerGUIs.keySet()) {
			GUI g = getPlayerGUI(name);
			if(!gui.equals(g)) continue;
			players.add(Bukkit.getPlayer(name));
		}
		
		return players;
	}
	
	public void setQuickBarGUI(String name, QuickBarGUI gui) {
		quickBarGUIs.put(name.toLowerCase(), gui);
	}
	
	public void setQuickBarGUI(Player p, QuickBarGUI gui) {
		setQuickBarGUI(p.getName(), gui);
	}
	
	public void removeQuickBarGUI(String name) {
		quickBarGUIs.remove(name.toLowerCase());
	}
	
	public void removeQuickBarGUI(Player p) {
		removeQuickBarGUI(p.getName());
	}
	
	public void clearQuickBarGUI(Player p) {
		if(!hasQuickBarGUI(p)) return;
		removeQuickBarGUI(p);
		if(p == null) return;
		p.getInventory().clear();
	}
	
	public QuickBarGUI getQuickBarGUI(String name) {
		return quickBarGUIs.get(name.toLowerCase());
	}
	
	public QuickBarGUI getQuickBarGUI(Player p) {
		return getQuickBarGUI(p.getName());
	}
	
	public boolean hasQuickBarGUI(String name) {
		return quickBarGUIs.containsKey(name.toLowerCase());
	}
	
	public boolean hasQuickBarGUI(Player p) {
		return hasQuickBarGUI(p.getName());
	}
	
	public List<Player> getOnlinePlayers(QuickBarGUI gui) {
		List<Player> players = new ArrayList<>();
		
		for(String name : quickBarGUIs.keySet()) {
			if(!gui.equals(getQuickBarGUI(name))) continue;
			Player player = Bukkit.getPlayer(name);
			if(player == null) continue;
			players.add(player);
		}
		
		return players;
	}
	
	public List<String> getPlayers(QuickBarGUI gui) {
		List<String> players = new ArrayList<>();
		
		for(String name : quickBarGUIs.keySet()) {
			if(!gui.equals(getQuickBarGUI(name))) continue;
			players.add(name);
		}
		
		return players;
	}
	
} 