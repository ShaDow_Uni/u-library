package shadowuni.plugin.library.api.lib;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import lombok.Getter;

public class VaultHandler {
	
	@Getter
    private Permission permission;
	@Getter
    private Economy economy;
	@Getter
    private Chat chat;
	
    public boolean setupPermission() {
        RegisteredServiceProvider<Permission> rsp = Bukkit.getServicesManager().getRegistration(Permission.class);
        if (rsp != null) {
            permission = rsp.getProvider();
        }
        return rsp != null;
    }

    public boolean setupEconomy() {
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServicesManager().getRegistration(Economy.class);
        if (rsp != null) {
            economy = rsp.getProvider();
        }
        return rsp != null;
    }

	public boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = Bukkit.getServicesManager().getRegistration(Chat.class);
        if (rsp != null) {
            chat = rsp.getProvider();
        }
        return rsp != null;
    }
	
	public double getMoney(Player p) {
		return getMoney(p.getName());
	}
	
	public double getMoney(String name) {
		return getEconomy().hasAccount(name) ? getEconomy().getBalance(name) : 0;
	}
	
	public boolean hasMoney(Player p, double money) {
		return hasMoney(p.getName(), money);
	}
	
	public boolean hasMoney(String name, double money) {
		return getEconomy().hasAccount(name) && getEconomy().has(name, money);
	}
	
	public void giveMoney(Player p, double money) {
		giveMoney(p.getName(), money);
	}
	
	public void giveMoney(String name, double money) {
		getEconomy().depositPlayer(name, money);
	}
	
	public boolean takeMoney(Player p, double money) {
		return takeMoney(p.getName(), money);
	}
	
	public boolean takeMoney(String name, double money) {
		return getEconomy().withdrawPlayer(name, money).transactionSuccess();
	}

}