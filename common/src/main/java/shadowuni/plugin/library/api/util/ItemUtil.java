package shadowuni.plugin.library.api.util;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ItemUtil {
	
	public static ItemStack getItem(String itemCode) {
		ItemStack item = null;
		
		if(itemCode.contains(":")) {
			String[] items = itemCode.split(":");
			item = new ItemStack(Integer.parseInt(items[0]));
			item.setDurability(Short.parseShort(items[1]));
		} else {
			item = new ItemStack(Integer.parseInt(itemCode));
		}
		
		return item;
	}
	
	public static String getItemCode(ItemStack item) {
		return item.getDurability() == 0 ? String.valueOf(item.getTypeId()) : item.getTypeId() + ":" + item.getDurability();
	}
	
	public static String getItemCode(Block block) {
		return block.getData() == 0 ? String.valueOf(block.getTypeId()) : block.getTypeId() + ":" + block.getData();
	}
	
	public static ItemStack makeItem(int itemCode, short durability, String displayName, String...lore) {
		ItemStack item = new ItemStack(itemCode);
		item.setDurability(durability);
		ItemMeta im = item.getItemMeta();
		if(displayName != null) {
			im.setDisplayName(ChatColor.WHITE + displayName);
		}
		if(lore != null && lore.length > 0) {
			for(int i = 0; i < lore.length; i++) {
				lore[i] = ChatColor.WHITE + lore[i];
			}
			im.setLore(Arrays.asList(lore));
		}
		item.setItemMeta(im);
		return item;
	}
	
	public static ItemStack makeItem(int itemCode, String displayName, String...lore) {
		ItemStack item = new ItemStack(itemCode);
		ItemMeta im = item.getItemMeta();
		if(displayName != null) {
			im.setDisplayName(ChatColor.WHITE + displayName);
		}
		if(lore != null && lore.length > 0) {
			for(int i = 0; i < lore.length; i++) {
				lore[i] = ChatColor.WHITE + lore[i];
			}
			im.setLore(Arrays.asList(lore));
		}
		item.setItemMeta(im);
		return item;
	}
	
	public static ItemStack makeItem(String itemCode, String displayName, String...lore) {
		ItemStack item = null;
		
		if(itemCode.contains(":")) {
			String[] items = itemCode.split(":");
			item = new ItemStack(Integer.parseInt(items[0]));
			item.setDurability(Short.parseShort(items[1]));
		} else {
			item = new ItemStack(Integer.parseInt(itemCode));
		}
		
		ItemMeta im = item.getItemMeta();
		if(displayName != null) {
			im.setDisplayName(ChatColor.WHITE + displayName);
		}
		if(lore != null && lore.length > 0) {
			for(int i = 0; i < lore.length; i++) {
				lore[i] = ChatColor.WHITE + lore[i];
			}
			im.setLore(Arrays.asList(lore));
		}
		item.setItemMeta(im);
		
		return item;
	}
	
	public static ItemStack makeItem(String itemCode, String displayName) {
		return makeItem(itemCode, displayName, null);
	}
	
	public static ItemStack makeItem(Material item, short durability, String displayName, String...lore) {
		return makeItem(item.getId(), durability, displayName, lore);
	}
	
	public static ItemStack makeItem(int itemCode, short durability, String displayName) {
		return makeItem(itemCode, durability, displayName, null);
	}
	
	public static ItemStack makeItem(Material item, short durability, String displayName) {
		return makeItem(item.getId(), durability, displayName, null);
	}
	
	public static ItemStack makeItem(Material item, String displayName, String...lore) {
		return makeItem(item.getId(), displayName, lore);
	}
	
	public static ItemStack makeItem(int itemCode, String displayName) {
		return makeItem(itemCode, displayName, null);
	}
	
	public static ItemStack makeItem(Material item, String displayName) {
		return makeItem(item.getId(), displayName, null);
	}
	
	public static int takeItem(Inventory inventory, ItemStack item, int amount) {
		int take = 0;
		
		for(int i = 0; i < inventory.getSize(); i++) {
			ItemStack ii = inventory.getItem(i);
			
			if(ii == null || !ii.isSimilar(item)) continue;
			
			if(ii.getAmount() > amount) {
				take += amount;
				
				ii.setAmount(ii.getAmount() - amount);
				break;
			} else {
				take += ii.getAmount();
				
				inventory.setItem(i, null);
				if((amount =- ii.getAmount()) < 1) break;
			}
		}
		
		return take;
	}
	
	public static int takeItemInHand(Player p, int amount) {
		ItemStack item = p.getItemInHand();
		
		if(item == null) return 0;
		
		int take = 0;
		
		if(item.getAmount() > amount) {
			take = amount;
			
			item.setAmount(item.getAmount() - amount);
		} else {
			take = item.getAmount();
			
			p.setItemInHand(null);
		}
		
		return take;
	}
	
	public static boolean equalsItem(ItemStack f, ItemStack s) {
		ItemStack o = f.clone();
		ItemStack a = s.clone();
		o.setAmount(1);
		a.setAmount(1);
		o.setDurability((short) 1);
		a.setDurability((short) 1);
		return o.equals(a);
	}
	
	public static boolean hasItem(Inventory inventory, ItemStack item) {
		for(ItemStack ii : inventory) {
			if(ii == null || equalsItem(ii, item)) return true;
		}
		return false;
	}
	
	public static int getItemAmount(Inventory inventory, ItemStack item) {
		int amount = 0;
		for(ItemStack ii : inventory) {
			if(ii == null || !equalsItem(ii, item)) continue;
			amount += ii.getAmount();
		}
		return amount;
	}
	
	public static int getEmptySlotAmount(Inventory inventory) {
		int amount = 0;
		for(ItemStack item : inventory) {
			if(!(item == null || item.getType() == Material.AIR)) continue;
			amount++;
		}
		return amount;
	}
	
	public static int getInventorySpace(Inventory inventory, ItemStack item) {
		int space = 0;
		
		for(ItemStack ii : inventory) {
			if(ii == null || ii.getType() == Material.AIR) {
				space += 64;
			} else if(ii.isSimilar(item)) {
				space += 64 - ii.getAmount();
			}
		}
		
		return space;
	}
	
	public static boolean hasInventorySpace(Inventory inventory, ItemStack item) {
		return getInventorySpace(inventory, item) >= item.getAmount();
	}
	
	public static ItemStack addEnchantOnEnchantBook(ItemStack enchantBook, Enchantment enchant, int level) {
		EnchantmentStorageMeta eMeta = (EnchantmentStorageMeta) enchantBook.getItemMeta();
		eMeta.addStoredEnchant(enchant, level, false);
		enchantBook.setItemMeta(eMeta);
		return enchantBook;
	}
	
	public static ItemStack addEnchantOnEnchantBook(ItemStack enchantBook, String enchantName, int level) {
		return addEnchantOnEnchantBook(enchantBook, Enchantment.getByName(enchantName), level);
	}
	
}