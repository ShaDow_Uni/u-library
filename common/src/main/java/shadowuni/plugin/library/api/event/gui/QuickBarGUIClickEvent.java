package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.category.ClickAction;
import shadowuni.plugin.library.api.event.UEvent;
import shadowuni.plugin.library.api.gui.QuickBarGUI;

public class QuickBarGUIClickEvent extends UEvent {
	
	@Getter
	private final Player player;
	
	@Getter
	private final ClickAction clickAction;
	
	@Getter
	private QuickBarGUI GUI;
	
	@Getter
	private PlayerInteractEvent playerInteractEvent;
	
	public QuickBarGUIClickEvent(PlayerInteractEvent playerInteractEvent) {
		this.playerInteractEvent = playerInteractEvent;
		player = playerInteractEvent.getPlayer();
		clickAction = (playerInteractEvent.getAction() == Action.LEFT_CLICK_AIR || playerInteractEvent.getAction() == Action.LEFT_CLICK_BLOCK) ? ClickAction.LEFT_CLICK : ClickAction.RIGHT_CLICK;
		GUI = ULibrary.getGUIManager().getQuickBarGUI(player);
	}
	
	public ItemStack getClickedItem() {
		return player.getInventory().getItemInHand();
	}
	
	public int getClickedX() {
		return  player.getInventory().getHeldItemSlot() + 1;
	}
	
}