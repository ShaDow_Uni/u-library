package shadowuni.plugin.library.api.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.time.FastDateFormat;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtil {
	
	public static String locationToString(Location location) {
		return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ() + ", " + location.getYaw() + ", " + location.getPitch();
	}
	
	public static Location stringToLocation(String str) {
		String[] strs = str.split(", ");
		return new Location(Bukkit.getWorld(strs[0]), Double.parseDouble(strs[1]), Double.parseDouble(strs[2]), Double.parseDouble(strs[3]), Float.parseFloat(strs[4]), Float.parseFloat(strs[5]));
	}
	
	public static String connectString(List<String> args, String connectChar) {
		return connectString(args.toArray(new String[args.size()]), connectChar);
	}
	
	public static String connectString(String[] args, String connectChar) {
		return connectString(args, 0, connectChar);
	}
	
	public static String connectString(String[] args, int start, String connectChar) {
		StringBuilder sb = new StringBuilder();
		
		for(int i = start; i < args.length; i++) {
			if(args[i] == null) continue;
			sb.append((sb.length() < 1 ? "" : connectChar) + args[i]);
		}
		
		return sb.toString();
	}
	
	public static String connectString(Object[] args, String connectChar) {
		return connectString(args, 0, connectChar);
	}
	
	public static String connectString(Object[] args, int start, String connectChar) {
		StringBuilder sb = new StringBuilder();
		
		for(int i = start; i < args.length; i++) {
			if(args[i] == null) continue;
			sb.append((sb.length() < 1 ? "" : connectChar) + args[i]);
		}
		
		return sb.toString();
	}
	
	public static String repeatString(String repeatChar, int number) {
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < number; i++) {
			sb.append(repeatChar);
		}
		
		return sb.toString();
	}
	
	public static String buildDateString(long time, String pattern) {
		Date date = new Date(time);
		return FastDateFormat.getInstance(pattern).format(date);
	}
	
	public static String buildTimeString(long time) {
		String[] times = new String[4];
		times[0] = (int) (time / 86400000) < 1 ? null : (int) (time / 86400000) + "일";
		times[1] = (int) (time / 3600000 % 24) < 1 ? null : (int) (time / 3600000 % 24) + "시";
		times[2] = (int) (time / 60000 % 60) < 1 ? null : (int) (time / 60000 % 60) + "분";
		times[3] = (int) (time / 1000 % 60) < 1 ? null : (int) (time / 1000 % 60) + "초";
		
		return connectString(times, " ");
	}
	
	public static List<String> getValue(String key, String text) {
		String ct = text;
		
		List<String> values = new ArrayList<>();
		
		Pattern pattern = Pattern.compile("<" + key + ":\\S+>");
		
		while(pattern.matcher(ct).find()) {
			int startIndex = ct.indexOf("<" + key + ":");
			int endIndex = ct.indexOf(">", startIndex);
			
			String value = ct.substring(startIndex + (2 + key.length()), endIndex);
			ct = ct.replace(ct.subSequence(startIndex, endIndex), "");
			
			values.add(value);
		}
		
		return values;
	}
	
	public static boolean hasValue(String key, String text) {
		Pattern pattern = Pattern.compile("<" + key + ":\\S+>");
		
		return pattern.matcher(text).find();
	}
	
}