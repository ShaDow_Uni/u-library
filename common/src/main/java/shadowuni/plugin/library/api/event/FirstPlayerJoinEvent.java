package shadowuni.plugin.library.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class FirstPlayerJoinEvent extends UEvent {
	
	private final Player player;
	
	private final PlayerJoinEvent playerJoinEvent;
	
}