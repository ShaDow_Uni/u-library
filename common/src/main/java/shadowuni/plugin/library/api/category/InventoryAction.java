package shadowuni.plugin.library.api.category;

public enum InventoryAction {
	LEFT_HOLD, LEFT_PLACE, RIGHT_HOLD, RIGHT_PLACE, SHIFT_CLICK, DOUBLE_CLICK, WHEEL, DROP, NOTHING, OTHER
}