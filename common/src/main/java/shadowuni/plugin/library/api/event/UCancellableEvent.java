package shadowuni.plugin.library.api.event;

import org.bukkit.event.Cancellable;

import lombok.Getter;
import lombok.Setter;

public class UCancellableEvent extends UEvent implements Cancellable {
	
	@Setter
	@Getter
	private boolean cancelled = false;
	
}