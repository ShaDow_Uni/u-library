package shadowuni.plugin.library.api.sql;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import lombok.Cleanup;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import shadowuni.plugin.library.api.ULibrary;

public class SQLManagerBase {
	
	protected ULibrary lib = ULibrary.getInstance();
	
	@Setter
	@Getter
	private String SQLAddress, SQLDatabase, SQLUser, SQLPassword, SQLTablePrefix;
	
	@Setter
	@Getter
	private int SQLPort;
	
	@Setter
	@Getter
	private boolean use, useUseOption;
	
	@Getter
	private FileConfiguration fileConfig;
	
	@Getter
	private SQLConfig SQLConfig;
	
	protected Connection conn;
	
	public void loadFileConfig(Plugin plugin) {
		createFileConfig(plugin);
		fileConfig = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "mysql-config.yml"));
		if(useUseOption) {
			use = fileConfig.getBoolean("사용");
		}
		SQLAddress = fileConfig.getString("주소");
		SQLPort = fileConfig.getInt("포트");
		SQLDatabase = fileConfig.getString("데이터베이스");
		SQLUser = fileConfig.getString("유저");
		SQLPassword = fileConfig.getString("비밀번호");
		SQLTablePrefix = fileConfig.getString("테이블 접두사");
		loadFileConfigOthers();
		lib.log("MySQL 설정을 불러왔습니다.");
	}
	
	@SneakyThrows(IOException.class)
	private void createFileConfig(Plugin plugin) {
		File file = new File(plugin.getDataFolder(), "mysql-config.yml");
		if(file.exists()) return;
		fileConfig = new YamlConfiguration();
		if(useUseOption) {
			fileConfig.set("사용", false);
		}
		fileConfig.set("주소", "localhost");
		fileConfig.set("포트", 3306);
		fileConfig.set("데이터베이스", "database");
		fileConfig.set("유저", "root");
		fileConfig.set("비밀번호", "password");
		fileConfig.set("테이블 접두사", "u_");
		createFileConfigOthers();
		fileConfig.save(file);
	}
	
	public void loadFileConfigOthers() { }
	public void createFileConfigOthers() { }
	
	public boolean connect(Plugin plugin) {
		try {
			loadFileConfig(plugin);
			if(useUseOption && !use) return false;
			conn = DriverManager.getConnection("jdbc:mysql://" + SQLAddress + ":" + SQLPort + "/" + SQLDatabase + "?autoReconnect=true", SQLUser, SQLPassword);
			SQLConfig = new SQLConfig(this);
			createTable();
			lib.log("MySQL에 접속되었습니다.");
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			lib.log("MySQL에 연결할 수 없습니다.");
			return false;
		}
	}
	
	public void close() {
		try {
			if(conn == null) return;
			conn.close();
			lib.log("MySQL과의 연결을 종료했습니다.");
		} catch(Exception e) {
			lib.log("MySQL과의 연결을 종료하는 중 오류가 발생했습니다.");
		}
	}
	
	public boolean isConnected() {
		return conn != null;
	}
	
	@SneakyThrows(SQLException.class)
	public void update(String sql) {
		@Cleanup PreparedStatement state = conn.prepareStatement(sql.replace("\\", "\\\\"));
		state.executeUpdate();
	}
	
	public void updatef(String sql, Object...args) {
		update(String.format(sql, args));
	}
	
	@SneakyThrows(SQLException.class)
	public PreparedStatement getPreparedStatement(String sql) {
		return conn.prepareStatement(sql);
	}
	
	public PreparedStatement getPreparedStatementf(String sql, Object...args) {
		return getPreparedStatement(String.format(sql, args));
	}
	
	public String getTableName(String tableName) {
		return SQLTablePrefix + tableName;
	}
	
	public void createTable() { }
	
	public void createTable(String name, String calumn) {
		update("create table if not exists " + getTableName(name) + " (" + calumn + ")");
	}
	
	public void deleteTable(String name) {
		update("drop table " + getTableName(name));
	}
	
	public void truncateTable(String name) {
		update("truncate table " + getTableName(name));
	}
	
}