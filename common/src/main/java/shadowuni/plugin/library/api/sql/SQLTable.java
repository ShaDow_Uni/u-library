package shadowuni.plugin.library.api.sql;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import shadowuni.plugin.library.api.util.StringUtil;

@Getter
public class SQLTable {
	
	@NonNull
	private final SQLManagerBase SQLManager;
	
	@NonNull
	private final String name, calumnString;
	
	private final List<SQLColumn> calumns;
	
	public SQLTable(SQLManagerBase SQLManager, String name, String calumn) {
		this.SQLManager = SQLManager;
		
		this.name = name;
		this.calumnString = calumn;
		calumns = new ArrayList<>();
		
		String[] cs = calumn.split(",");
		for(int i = 0; i < cs.length; i++) {
			String c = cs[i].trim();
			
			if(c.toLowerCase().startsWith("primary key")) {
				String keyStr = c.substring("primary key(".length(), c.length() - 1);
				
				for(String key : keyStr.split(",")) {
					SQLColumn cm = getCalumn(key.trim());
					if(cm == null) continue;
					
					cm.setPrimaryKey(true);
				}
				
				break;
			}
			
			String[] ccs = c.split(" ");
			
			String cName = ccs[0];
			String cType = ccs[1];
			boolean primaryKey = c.toLowerCase().endsWith("primary key");
			
			SQLColumn sc = new SQLColumn(cName, cType, primaryKey);
			calumns.add(sc);
		}
	}
	
	public SQLColumn getCalumn(String name) {
		for(SQLColumn ac : calumns) {
			if(ac.getName().equalsIgnoreCase(name)) return ac;
		}
		
		return null;
	}
	
	public String getTableName() {
		return SQLManager.getSQLTablePrefix() + name;
	}
	
	public SQLTable createTable() {
		SQLManager.createTable(name, calumnString);
		return this;
	}
	
	public void deleteTable() {
		SQLManager.deleteTable(name);
	}
	
	public void truncateTable() {
		SQLManager.truncateTable(name);
	}
	
	public void insert(Object...values) {
		for (int i = 0; i < values.length; i++) {
			if(values[i] == null) {
				values[i] = "null";
			} else if(values[i].toString().startsWith("$$")) {
				values[i] = values[i].toString().substring(2);
			} else if(!(values[i] instanceof String)) continue;
			values[i] = "'" + values[i] + "'";
		}
		
		SQLManager.update("insert into " + getTableName() + " values (" + StringUtil.connectString(values, ",") + ")");
	}
	
	public void insertIgnore(Object...values) {
		for (int i = 0; i < values.length; i++) {
			if(values[i] == null) {
				values[i] = "null";
			} else if(values[i].toString().startsWith("$$")) {
				values[i] = values[i].toString().substring(2);
			} else if(!(values[i] instanceof String)) continue;
			values[i] = "'" + values[i] + "'";
		}
		
		SQLManager.update("insert ignore into " + getTableName() + " values (" + StringUtil.connectString(values, ",") + ")");
	}
	
	public void insertDuplicate(Object...values) {
		for (int i = 0; i < values.length; i++) {
			if(values[i] == null) {
				values[i] = "null";
			} else if(values[i] instanceof String && !values[i].toString().startsWith("$$")) {
				values[i] = "'" + values[i] + "'";
			} else if(values[i] instanceof Boolean) {
				values[i] = (boolean) values[i] ? 1 : 0;
			}
		}
		
		List<String> ds = new ArrayList<>();
		for (int i = 0; i < calumns.size(); i++) {
			SQLColumn c = calumns.get(i);
			if(c.isPrimaryKey()) continue;
			
			String v = "=" + (values[i].toString().startsWith("$$") ? c.getName() : values[i].toString());
			
			ds.add(c.getName() + v);
		}
		
		for (int i = 0; i < values.length; i++) {
			if(!values[i].toString().startsWith("$$")) continue;
			values[i] = values[i].toString().substring(2);
		}
		
		SQLManager.update("insert into " + getTableName() + " values (" + StringUtil.connectString(values, ",") + ")"
				+ " on duplicate key update " + StringUtil.connectString(ds, ","));
	}
	
	public void update(String set) {
		SQLManager.update("update " + getTableName() + " set " + set);
	}
	
	public void update(String set, String sql) {
		SQLManager.update("update " + getTableName() + " set " + set + " " + sql);
	}
	
	public void delete() {
		SQLManager.update("delete from " + getTableName());
	}
	
	public void delete(String sql) {
		SQLManager.update("delete from " + getTableName() + " " + sql);
	}
	
	public PreparedStatement select(String selected) {
		return SQLManager.getPreparedStatement("select " + selected + " from " + getTableName());
	}
	
	public PreparedStatement select(String selected, String sql) {
		return SQLManager.getPreparedStatement("select " + selected + " from " + getTableName() + " " + sql);
	}
	
}