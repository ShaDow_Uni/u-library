package shadowuni.plugin.library.api.category;

public enum ClickAction {
	LEFT_CLICK, RIGHT_CLICK
}