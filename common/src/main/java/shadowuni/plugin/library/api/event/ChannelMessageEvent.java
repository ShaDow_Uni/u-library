package shadowuni.plugin.library.api.event;

import com.google.common.io.ByteArrayDataInput;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ChannelMessageEvent extends UEvent {
	
	@Getter
	private final String key, task;
	
	@Getter
	private final ByteArrayDataInput byteArrayDataInput;
	
}