package shadowuni.plugin.library.api.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Cleanup;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

public class SQLManager extends SQLManagerBase {
	
	@Setter
	@Getter
	private boolean useUpload;
	
	@Getter
	private SQLTable playerKeyTable;
	
	public SQLManager() {
		setUseUseOption(true);
	}
	
	@Override
	public void createTable() {
		playerKeyTable = new SQLTable(this, "PlayerKey", "player_key varchar(36) primary key, name varchar(16)").createTable();
	}
	
	@Override
	public void createFileConfigOthers() {
		getFileConfig().set("업로드", false);
	}
	
	@Override
	public void loadFileConfigOthers() {
		useUpload = getFileConfig().getBoolean("업로드");
	}
	
	public void savePlayerKey(String name, String playerKey) {
		playerKeyTable.insertDuplicate(playerKey, name);
	}
	
	@SneakyThrows(SQLException.class)
	public String getPlayerKey(String name) {
		@Cleanup PreparedStatement state = playerKeyTable.select("player_key", "where name='" + name + "'");
		@Cleanup ResultSet result = state.executeQuery();
		
		return result.next() ? result.getString("player_key") : null;
	}
	
	@SneakyThrows(SQLException.class)
	public boolean hasPlayerKey(String name) {
		@Cleanup PreparedStatement state = playerKeyTable.select("player_key", "where name='" + name + "'");
		@Cleanup ResultSet result = state.executeQuery();
		
		return result.next();
	}
	
}