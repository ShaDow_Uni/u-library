package shadowuni.plugin.library.api.util;

import java.util.ArrayList;
import java.util.List;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NumberUtil {
	
	public static int getInteger(String str) {
		try {
			return Integer.parseInt(str);
		} catch(NumberFormatException e) {
			return -1;
		}
	}
	
	public static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch(NumberFormatException e) {
			return false;
		}
	}
	
	public List<Integer> getIntegers(String str) {
		List<Integer> l = new ArrayList<>();
		
		if(str.contains("~")) {
			String[] s = str.split("~");
			
			int min = Integer.parseInt(s[0]);
			int max = Integer.parseInt(s[1]);
			
			for(; min <= max; min++) {
				l.add(min);
			}
		} else {
			l.add(Integer.parseInt(str));
		}
		
		return l;
	}
	
}