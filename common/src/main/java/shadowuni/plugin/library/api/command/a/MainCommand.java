package shadowuni.plugin.library.api.command.a;

import org.bukkit.command.CommandSender;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.UPlugin;

@Setter
@Getter
public class MainCommand extends Command {
	
	public MainCommand(UPlugin plugin, String name) {
		super(plugin, name);
	}
	
	@Override
	public void sendUsage(CommandSender sender, boolean format) {
		String r = "/" + name + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
			return;
		}
		
		ULibrary.nmsg(sender, r);
	}
	
	@Override
	public void sendUsage(CommandSender sender, String entered, boolean format) {
		String r = "/" + entered + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
			return;
		}
		
		ULibrary.nmsg(sender, r);
	}
	
	@Override
	public boolean sendUsageIfHasPermission(CommandSender sender, boolean format) {
		if(getPermission() != null && !sender.hasPermission(getPermission())) return false;
		
		String r = "/" + name + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
		} else {
			ULibrary.nmsg(sender, r);
		}
		
		return true;
	}
	
	@Override
	public boolean sendUsageIfHasPermission(CommandSender sender, String entered, boolean format) {
		if(permission != null && !sender.hasPermission(permission)) return false;
		
		String r = "/" + entered + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
		} else {
			ULibrary.nmsg(sender, r);
		}
		
		return true;
	}
	
}