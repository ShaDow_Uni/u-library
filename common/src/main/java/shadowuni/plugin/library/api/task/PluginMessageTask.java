package shadowuni.plugin.library.api.task;

import org.bukkit.plugin.Plugin;

import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.scheduler.UBukkitRunnable;

public class PluginMessageTask extends UBukkitRunnable {
	
	private final Plugin plugin;
	private final String channel;
	private final byte[] bytes;
	
	public PluginMessageTask(Plugin plugin, String channel, byte[] bytes) {
		super(plugin);
		
		this.plugin = plugin;
		this.channel = channel;
		this.bytes = bytes;
	}
	
	public void run() {
		if(ULibrary.getOnlinePlayers().size() < 1) return;
		
		ULibrary.getOnlinePlayers().get(0).sendPluginMessage(plugin, channel, bytes);
	}

}
