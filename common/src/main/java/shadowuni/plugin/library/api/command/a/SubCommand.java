package shadowuni.plugin.library.api.command.a;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.UPlugin;

@Setter
public class SubCommand extends Command {
	
	@Getter
	private String parentCommand;
	
	public SubCommand(UPlugin plugin, String name, String parent) {
		super(plugin, name);
		parentCommand = parent;
	}
	
	public String getCommand() {
		return getParentCommand() + " " + getName();
	}
	
	public List<String> getAliasesCommands() {
		List<String> a = new ArrayList<>();
		
		for(String s : getAliases()) {
			a.add(getParentCommand() + " " + s);
		}
		
		return a;
	}
	
	@Override
	public void sendUsage(CommandSender sender, boolean format) {
		String r = "/" + getCommand() + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
			return;
		}
		ULibrary.nmsg(sender, r);
	}
	
	@Override
	public void sendUsage(CommandSender sender, String entered, boolean format) {
		String r = "/" + entered + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
			return;
		}
		
		ULibrary.nmsg(sender, r);
	}
	
	@Override
	public boolean sendUsageIfHasPermission(CommandSender sender, boolean format) {
		if(getPermission() != null && !sender.hasPermission(getPermission())) return false;
		
		String r = "/" + getCommand() + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
		} else {
			ULibrary.nmsg(sender, r);
		}
		
		return true;
	}
	
	@Override
	public boolean sendUsageIfHasPermission(CommandSender sender, String entered, boolean format) {
		if(permission != null && !sender.hasPermission(permission)) return false;
		
		String r = "/" + entered + (additional == null ? "" : " " + additional) + plugin.getColor() + " - " + usage;
		
		if(format) {
			ULibrary.msg(sender, getPlugin().getPluginPackage(), r);
		} else {
			ULibrary.nmsg(sender, r);
		}
		
		return true;
	}
	
}