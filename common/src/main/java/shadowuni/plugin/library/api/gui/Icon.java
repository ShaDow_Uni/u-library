package shadowuni.plugin.library.api.gui;

import java.util.HashMap;

import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@RequiredArgsConstructor
public abstract class Icon {
	
	private final String key;
	
	@Setter
	private ItemStack item;
	
	private HashMap<String, Object> ETCs = new HashMap<>();
	
	public Icon() {
		key = null;
	}
	
	public void update() {
		item = updateItem();
	}
	
	public abstract ItemStack updateItem();
	
	public void setAmount(int amount) {
		item.setAmount(amount);
	}
	
	public void setETC(String key, Object value) {
		ETCs.put(key, value);
	}
	
	public boolean existsETC(String key) {
		return ETCs.containsKey(key);
	}
	
	public Object getETC(String key) {
		return ETCs.get(key);
	}
	
}