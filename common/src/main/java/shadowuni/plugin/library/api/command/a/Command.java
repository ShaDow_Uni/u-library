package shadowuni.plugin.library.api.command.a;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import shadowuni.plugin.library.api.UPlugin;

@Setter
@Getter
@RequiredArgsConstructor
public abstract class Command {
	
	protected final UPlugin plugin;
	
	protected final String name;
	protected List<String> aliases = new ArrayList<>();
	protected String additional, usage;
	
	protected int minArgs;
	
	protected String permission;
	protected String noPermissionMessage;
	
	protected String playerOnlyMessage;
	protected String consoleOnlyMessage;
	
	public abstract void sendUsage(CommandSender sender, boolean format);
	
	public abstract void sendUsage(CommandSender sender, String entered, boolean format);
	
	public abstract boolean sendUsageIfHasPermission(CommandSender sender, boolean format);
	
	public abstract boolean sendUsageIfHasPermission(CommandSender sender, String entered, boolean format);
	
}