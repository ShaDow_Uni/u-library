package shadowuni.plugin.library.api.command;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.UPlugin;

@RequiredArgsConstructor
public abstract class SubCommandBase {
	
	protected final ULibrary lib = ULibrary.getInstance();
	
	@Getter
	protected final UPlugin plugin;
	
	protected final MainCommandBase mainCommandHandler;
	
	@Getter
	private LinkedHashMap<String, String> helpMessages = new LinkedHashMap<>();
	
	@Getter
	private HashMap<String, String> permMessages = new HashMap<>(); // <Command, Permission>
	
	public abstract boolean onCommand(CommandSender sender, String label, String[] args);
	
	public boolean isConsole(CommandSender sender, boolean msg) {
		if(!(sender instanceof Player) && msg) {
			lib.log("콘솔에서는 사용할 수 없습니다!");
		}
		return !(sender instanceof Player);
	}
	
	public void registerHelpMessage(String command, String comment) {
		helpMessages.put(command, comment);
		mainCommandHandler.registerHelpMessage(command, comment);
	}
	
	public void registerHelpMessage(String command, String comment, String perm) {
		helpMessages.put(command, comment);
		permMessages.put(command, perm);
		mainCommandHandler.registerHelpMessage(command, comment, perm);
	}
	
	public void sendHelpMessage(CommandSender sender, String label, String command) {
		if(permMessages.containsKey(command) && !sender.hasPermission(permMessages.get(command))) return;
		lib.nmsg(sender, "");
		lib.nmsg(sender, ChatColor.WHITE + "/" + label + " " + command + ChatColor.GREEN + " - " + helpMessages.get(command));
	}
	
	public void sendHelpMessage(CommandSender sender, String label, int page) {
		lib.nmsg(sender, "");
		
		int maxPage = (int) Math.ceil(getPermittedCommandCount(sender) / 7) + 1;
		
		if(page > maxPage) {
			lib.msg(sender, plugin.getPluginPackage(), "1 ~ " + maxPage + "의 정수만 입력 가능합니다.");
			return;
		}
		
		lib.msg(sender, plugin.getPluginPackage(), " [ " + page + " / " + maxPage + " ]");
		
		for(int i = 0; i < 7; i++) {
			int num = i + ((page - 1) * 7);
			if(helpMessages.size() <= num) break;
			
			String cmd = ((String[]) helpMessages.keySet().toArray(new String[helpMessages.size()]))[num];
			
			if(permMessages.containsKey(cmd) && !sender.hasPermission(permMessages.get(cmd))) continue;
			lib.nmsg(sender, ChatColor.WHITE + "/" + label + " " + cmd + lib.getPluginColor(lib.getLastClassName()) + " - " + helpMessages.get(cmd));
		}
	}
	
	protected int getPermittedCommandCount(CommandSender sender) {
		int count = 0;
		
		for(String node : permMessages.values()) {
			if(!sender.hasPermission(node)) continue;
			count++;
		}
		
		return helpMessages.size() - (permMessages.size() - count);
	}
	
}