package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;

import lombok.Getter;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.event.UEvent;
import shadowuni.plugin.library.api.gui.GUI;

public class GUIOpenEvent extends UEvent {
	
	@Getter
	private final Player player;
	
	@Getter
	private final InventoryOpenEvent inventoryOpenEvent;
	
	@Getter
	private GUI GUI;
	
	public GUIOpenEvent(InventoryOpenEvent inventoryOpenEvent) {
		this.inventoryOpenEvent = inventoryOpenEvent;
		player = (Player) inventoryOpenEvent.getPlayer();
		GUI = ULibrary.getGUIManager().getPlayerGUI(player);
	}
	
}