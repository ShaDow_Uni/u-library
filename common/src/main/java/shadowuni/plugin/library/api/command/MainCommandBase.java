package shadowuni.plugin.library.api.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.UPlugin;

@RequiredArgsConstructor
public class MainCommandBase implements CommandExecutor {
	
	protected ULibrary lib = ULibrary.getInstance();
	
	@Getter
	protected final UPlugin plugin;
	
	@Getter
	private List<SubCommandBase> commands = new ArrayList<>();
	
	@Getter
	private LinkedHashMap<String, String> helpMessages = new LinkedHashMap<>();
	
	@Getter
	private HashMap<String, String> permMessages = new HashMap<>(); // <Command, Permission>
	
	public void registerCommand(SubCommandBase cb) {
		commands.add(cb);
	}
	
	public void unRegisterCommand(SubCommandBase cb) {
		commands.remove(cb);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		int n = 0;
		for(SubCommandBase command : commands) {
			if(!command.onCommand(sender, label, args)) n++;
		}
		
		if(n == commands.size()) {
			sendHelpMessage(sender, label,  1);
		}
		
		return true;
	}
	
	public void registerHelpMessage(String command, String comment) {
		helpMessages.put(command, comment);
	}
	
	public void registerHelpMessage(String command, String comment, String perm) {
		registerHelpMessage(command, comment);
		permMessages.put(command, perm);
	}
	
	public void sendHelpMessage(CommandSender sender, String label, int page) {
		lib.nmsg(sender, "");
		
		int maxPage = (int) Math.ceil(getPermittedCommandCount(sender) / 7) + 1;
		
		if(page > maxPage) {
			lib.msg(sender, "1 ~ " + maxPage + "의 정수만 입력 가능합니다.");
			return;
		}
		
		lib.msg(sender, " [ " + page + " / " + maxPage + " ]");
		
		for(int i = 0; i < 7; i++) {
			int num = i + ((page - 1) * 7);
			if(helpMessages.size() <= num) break;
			
			String cmd = ((String[]) helpMessages.keySet().toArray(new String[helpMessages.size()]))[num];
			
			if(permMessages.containsKey(cmd) && !sender.hasPermission(permMessages.get(cmd))) continue;
			lib.nmsg(sender, ChatColor.WHITE + "/" + label + " " + cmd + lib.getPluginColor(lib.getLastClassName()) + " - " + helpMessages.get(cmd));
		}
	}
	
	protected int getPermittedCommandCount(CommandSender sender) {
		int count = 0;
		
		for(String node : permMessages.values()) {
			if(!sender.hasPermission(node)) continue;
			count++;
		}
		
		return helpMessages.size() - (permMessages.size() - count);
	}
	
}