package shadowuni.plugin.library.api.sql;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public class SQLColumn {
	
	private final String name, type;
	
	@Setter
	private boolean primaryKey;

}