package shadowuni.plugin.library.api.util;

import org.bukkit.ChatColor;

import lombok.experimental.UtilityClass;
import shadowuni.plugin.library.api.category.DefaultFontInfo;

@UtilityClass
public class TextUtil {
	
	public static String getChatCentredText(String text) {
		if(text == null || text.equals("")) {
			return "";
		}
		
		text = ChatColor.translateAlternateColorCodes('&', text);
	 
	    int messagePxSize = 0;
	    boolean previousCode = false;
	    boolean isBold = false;
	 
	    for(char c : text.toCharArray()){
	        if(c == '§'){
	            previousCode = true;
	        } else if (previousCode){
	            previousCode = false;
	            isBold = c == 'l' || c == 'L';
	        } else {
	            DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
	            messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
	            messagePxSize++;
	        }
	    }
	    
	    int CENTER_PX = 154;
	    int halvedMessageSize = messagePxSize / 2;
	    int toCompensate = CENTER_PX - halvedMessageSize;
	    int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
	    int compensated = 0;
	    
	    StringBuilder sb = new StringBuilder();
	    while(compensated < toCompensate){
	        sb.append(" ");
	        compensated += spaceLength;
	    }
	    
	    return sb.toString() + text;
	}
	
	
	public static String fixFontSize(String text, int size) {
		String ret = text.toUpperCase();
		
		if ( text != null ) {
			for (int i=0; i < text.length(); i++) {
				if ( text.charAt(i) == 'I' || text.charAt(i) == ' ') {
					ret += " ";
				}
			}
			
			int faltaEspacos = size - text.length();
			faltaEspacos = (faltaEspacos * 2);
			
			for (int i=0; i < faltaEspacos; i++) {
				ret += " ";
			}
		}
		
	    return ret;
	}
	
}