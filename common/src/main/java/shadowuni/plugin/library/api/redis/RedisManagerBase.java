package shadowuni.plugin.library.api.redis;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import shadowuni.plugin.library.api.ULibrary;

public class RedisManagerBase {
	
	protected ULibrary lib = ULibrary.getInstance();
	
	@Setter
	@Getter
	private String redisAddress;
	
	@Setter
	@Getter
	private int redisPort;
	
	@Setter
	@Getter
	private boolean use, useUseOption;
	
	@Getter
	private FileConfiguration fileConfig;
	
	protected JedisPool pool;
	
	public void loadFileConfig(JavaPlugin plugin) {
		createFileConfig(plugin);
		fileConfig = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "redis-config.yml"));
		if(useUseOption) {
			use = fileConfig.getBoolean("사용");
		}
		redisAddress = fileConfig.getString("주소");
		redisPort = fileConfig.getInt("포트");
		loadFileConfigOthers();
		lib.log("Redis 설정을 불러왔습니다.");
	}
	
	@SneakyThrows(IOException.class)
	private void createFileConfig(JavaPlugin plugin) {
		File file = new File(plugin.getDataFolder(), "redis-config.yml");
		if(file.exists()) return;
		YamlConfiguration config = new YamlConfiguration();
		if(useUseOption) {
			config.set("사용", false);
		}
		config.set("주소", "localhost");
		config.set("포트", 6379);
		createFileConfigOthers();
		config.save(file);
	}
	
	public void loadFileConfigOthers() { }
	public void createFileConfigOthers() { }
	
	public boolean connect(JavaPlugin plugin) {
		try {
			loadFileConfig(plugin);
			if(useUseOption && !use) return false;
			pool = new JedisPool(redisAddress, redisPort);
			lib.log("Redis에 접속되었습니다.");
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			lib.log("Redis에 연결할 수 없습니다,");
			return false;
		}
	}
	
	public void close() {
		try {
			if(!isConnected()) return;
			pool.close();
			lib.log("Redis과의 연결을 종료했습니다.");
		} catch(Exception e) {
			lib.log("Redis과의 연결을 종료하는 중 오류가 발생했습니다.");
		}
	}
	
	public boolean isConnected() {
		return pool != null && !pool.isClosed();
	}
	
	public Jedis getResource() {
		return pool.getResource();
	}
	
}