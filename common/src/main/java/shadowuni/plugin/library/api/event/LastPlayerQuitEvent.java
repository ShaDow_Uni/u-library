package shadowuni.plugin.library.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class LastPlayerQuitEvent extends UEvent {
	
	private final Player player;
	
	private final PlayerQuitEvent playerQuitEvent;
	
}