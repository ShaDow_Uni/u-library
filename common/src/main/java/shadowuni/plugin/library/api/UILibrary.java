package shadowuni.plugin.library.api;

import org.bukkit.entity.Player;

public abstract class UILibrary {
	
	public abstract String getPlayerKey(String name);
	
	public abstract String getPlayerKey(Player player);
	
	public abstract Player getPlayerByPlayerKey(String playerKey);
	
}