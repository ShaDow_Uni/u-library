package shadowuni.plugin.library.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PlayerMoveLocationEvent extends UCancellableEvent {
	
	@Getter
	private final Player player;
	
	@Getter
	private final PlayerMoveEvent playerMoveEvent;
	
}