package shadowuni.plugin.library.api.permission;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.SneakyThrows;
import shadowuni.plugin.library.api.util.PluginUtil;

public class PermissionManager {
	
	public boolean registerPermissions(JavaPlugin plugin) {
		try {
			ZipInputStream jarStream = new ZipInputStream(new FileInputStream(PluginUtil.getFile(plugin)));
			ZipEntry item = null;
			
			while((item = jarStream.getNextEntry()) != null) {
				if(item.isDirectory() || !item.getName().endsWith(".class")) continue;
				String className = item.getName().replaceAll("/", ".");
				Class<?> c = Class.forName(className.substring(0, className.length() - 6));
				try {
					UPermissionList pl = (UPermissionList) c.newInstance();
					registerPermissions(plugin, pl);
					
					return true;
				} catch (Exception ex) { }
			}
			
			jarStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean registerPermissions(JavaPlugin plugin, String pack) {
		try {
			ZipInputStream jarStream = new ZipInputStream(new FileInputStream(PluginUtil.getFile(plugin)));
			ZipEntry item = null;
			
			while((item = jarStream.getNextEntry()) != null) {
				if(item.isDirectory() || !item.getName().endsWith(".class")) continue;
				String className = item.getName().replaceAll("/", ".").substring(0, item.getName().length() - 6);
				if(!className.substring(0, className.lastIndexOf(".")).equals(pack)) continue;
				Class<?> c = Class.forName(className);
				try {
					UPermissionList pl = (UPermissionList) c.newInstance();
					registerPermissions(plugin, pl);
					
					return true;
				} catch (Exception ex) { }
			}
			
			jarStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	@SneakyThrows(Exception.class)
	public static void registerPermissions(JavaPlugin plugin, UPermissionList permissionList) {
		for(Field field : permissionList.getClass().getFields()) {
			Bukkit.getPluginManager().addPermission(new Permission(field.get(permissionList).toString()));
		}
	}
	
}