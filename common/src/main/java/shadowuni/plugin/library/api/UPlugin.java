package shadowuni.plugin.library.api;

import java.io.IOException;
import java.net.URLClassLoader;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import shadowuni.plugin.library.api.util.PluginUtil;

public abstract class UPlugin extends JavaPlugin {
	
	protected ULibrary lib = ULibrary.getInstance();
	
	private boolean disable;
	
	@Getter
	protected String version;
	@Getter
	private String pluginPackage;
	
	@Setter
	@Getter
	private boolean deleteOnExit;
	
	public void onUEnable() {}
	public void onUDisable() {}
	
	public void onEnable() {
		disable = false;
		
		onUEnable();
		
		if(disable) return;
		
		version = getDescription().getVersion();
		log("플러그인이 활성화되었습니다. (v" + version + ")");
	}
	
	@SneakyThrows(IOException.class)
	public void onDisable() {
		onUDisable();
		if(isDeleteOnExit()) {
			((URLClassLoader) getClass().getClassLoader()).close();
			
			getFile().delete();
		}
		log("플러그인이 비활성화되었습니다. (v" + getDescription().getVersion() + ")");
	}
	
	public void disable() {
		disable = true;
		
		PluginUtil.disablePlugin(this);
	}
	
	protected void log(Object message) {
		lib.log(pluginPackage, message);
	}
	
	protected void wlog(Object message) {
		lib.wlog(pluginPackage, message);
	}
	
	protected void nlog(Object message) {
		lib.nlog(message);
	}
	
	protected void registerPrefix(String prefix) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerPrefix(pluginPackage, prefix);
	}
	
	protected void registerColor(ChatColor color) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerColor(pluginPackage, color);
	}
	
	protected void registerLogFormat(String format) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerLogFormat(pluginPackage, format);
	}
	
	protected void registerWarningLogFormat(String format) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerWarningLogFormat(pluginPackage, format);
	}
	
	protected void registerMessageFormat(String format) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerMessageFormat(pluginPackage, format);
	}
	
	protected void registerWarningMessageFormat(String format) {
		pluginPackage = lib.getLastClassName();
		pluginPackage = pluginPackage.substring(0, pluginPackage.lastIndexOf("."));
		lib.registerWarningMessageFormat(pluginPackage, format);
	}
	
	protected int registerListeners() {
		return PluginUtil.registerListeners(this);
	}
	
	protected int registerListeners(String pack) {
		return PluginUtil.registerListeners(this, pack);
	}
	
	protected void registerCommand(CommandExecutor executor, String... commands) {
		PluginUtil.registerCommand(this, executor, commands);
	}
	
	protected boolean registerPermissions() {
		return lib.getPermissionManager().registerPermissions(this);
	}
	
	protected boolean registerPermissions(String pack) {
		return lib.getPermissionManager().registerPermissions(this, pack);
	}
	
	public String getPrefix() {
		return lib.getPrefix(pluginPackage);
	}
	
	public ChatColor getColor() {
		return lib.getPluginColor(pluginPackage);
	}
	
	protected String getMD5() {
		return PluginUtil.getMD5(this);
	}
	
	protected Plugin getPlugin(String name) {
		return Bukkit.getPluginManager().getPlugin(name);
	}
	
	protected boolean existsPlugin(String name) {
		return getPlugin(name) != null;
	}
	
}