package shadowuni.plugin.library.api.lib.barapi;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;

import lombok.Getter;
import lombok.Setter;
import protocolsupport.api.ProtocolSupportAPI;
import protocolsupport.api.ProtocolVersion;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.category.NMSVersion;
import shadowuni.plugin.library.api.lib.barapi.nms.FakeDragon;
import shadowuni.plugin.library.api.lib.barapi.nms.v1_9P;
import shadowuni.plugin.library.api.util.ReflectionUtil;

/**
 * Allows plugins to safely set a health bar message.
 *
 * @author James Mortemore
 */

@Setter
@Getter
public class BarAPI implements Listener {

	private boolean useProtocolSupport, useSpigotHack;
	
	private HashMap<UUID, FakeDragon> players = new HashMap<UUID, FakeDragon>();
	private HashMap<UUID, Integer> timers = new HashMap<UUID, Integer>();

	/**
	 * Set a message for all players.<br>
	 * It will remain there until the player logs off or another plugin overrides it.<br>
	 * This method will show a full health bar and will cancel any running timers.
	 *
	 * @param message The message shown.<br>
	 *								Due to limitations in Minecraft this message cannot be longer than 64 characters.<br>
	 *								It will be cut to that size automatically.
	 *
	 * @see BarAPI#setMessage(player, message)
	 */

	public void setMessage(String message) {
		for (Player player : ULibrary.getOnlinePlayers()) {
			setMessage(player, message);
		}
	}

	/**
	 * Set a message for the given player.<br>
	 * It will remain there until the player logs off or another plugin overrides it.<br>
	 * This method will show a full health bar and will cancel any running timers.
	 *
	 * @param player	The player who should see the given message.
	 * @param message The message shown to the player.<br>
	 *								Due to limitations in Minecraft this message cannot be longer than 64 characters.<br>
	 *								It will be cut to that size automatically.
	 */

	public void setMessage(Player player, String message) {
		if (hasBar(player))
			removeBar(player);
		FakeDragon dragon = getDragon(player, message);

		dragon.name = cleanMessage(message);
		dragon.health = dragon.getMaxHealth();

		cancelTimer(player);

		sendDragon(dragon, player);
	}

	/**
	 * Set a message for all players.<br>
	 * It will remain there for each player until the player logs off or another plugin overrides it.<br>
	 * This method will show a health bar using the given percentage value and will cancel any running timers.
	 *
	 * @param message The message shown to the player.<br>
	 *								Due to limitations in Minecraft this message cannot be longer than 64 characters.<br>
	 *								It will be cut to that size automatically.
	 * @param percent The percentage of the health bar filled.<br>
	 *								This value must be between 0F (inclusive) and 100F (inclusive).
	 *
	 * @throws IllegalArgumentException If the percentage is not within valid bounds.
	 * @see BarAPI#setMessage(player, message, percent)
	 */

	public void setMessage(String message, float percent) {
		for (Player player : ULibrary.getOnlinePlayers()) {
			setMessage(player, message, percent);
		}
	}

	/**
	 * Set a message for the given player.<br>
	 * It will remain there until the player logs off or another plugin overrides it.<br>
	 * This method will show a health bar using the given percentage value and will cancel any running timers.
	 *
	 * @param player	The player who should see the given message.
	 * @param message The message shown to the player.<br>
	 *								Due to limitations in Minecraft this message cannot be longer than 64 characters.<br>
	 *								It will be cut to that size automatically.
	 * @param percent The percentage of the health bar filled.<br>
	 *								This value must be between 0F (inclusive) and 100F (inclusive).
	 *
	 * @throws IllegalArgumentException If the percentage is not within valid bounds.
	 */

	public void setMessage(Player player, String message, float percent) {
		Validate.isTrue(0F <= percent && percent <= 100F, "Percent must be between 0F and 100F, but was: ", percent);

		if (hasBar(player))
			removeBar(player);

		FakeDragon dragon = getDragon(player, message);

		dragon.name = cleanMessage(message);
		dragon.health = (percent / 100f) * dragon.getMaxHealth();

		cancelTimer(player);

		sendDragon(dragon, player);
	}

	/**
	 * Set a message for all players.<br>
	 * It will remain there for each player until the player logs off or another plugin overrides it.<br>
	 * This method will use the health bar as a decreasing timer, all previously started timers will be cancelled.<br>
	 * The timer starts with a full bar.<br>
	 * The health bar will be removed automatically if it hits zero.
	 *
	 * @param message The message shown.<br>
	 *								Due to limitations in Minecraft this message cannot be longer than 64 characters.<br>
	 *								It will be cut to that size automatically.
	 * @param seconds The amount of seconds displayed by the timer.<br>
	 *								Supports values above 1 (inclusive).
	 *
	 * @throws IllegalArgumentException If seconds is zero or below.
	 * @see BarAPI#setMessage(player, message, seconds)
	 */

	public void setMessage(String message, int seconds) {
		for (Player player : ULibrary.getOnlinePlayers()) {
			setMessage(player, message, seconds);
		}
	}

	/**
	 * Set a message for the given player.<br>
	 * It will remain there until the player logs off or another plugin overrides it.<br>
	 * This method will use the health bar as a decreasing timer, all previously started timers will be cancelled.<br>
	 * The timer starts with a full bar.<br>
	 * The health bar will be removed automatically if it hits zero.
	 *
	 * @param player	The player who should see the given timer/message.
	 * @param message The message shown to the player.<br>
	 *								Due to limitations in Minecraft this message cannot be longer than 64 characters.<br>
	 *								It will be cut to that size automatically.
	 * @param seconds The amount of seconds displayed by the timer.<br>
	 *								Supports values above 1 (inclusive).
	 *
	 * @throws IllegalArgumentException If seconds is zero or below.
	 */

	public void setMessage(final Player player, String message, int seconds) {
		Validate.isTrue(seconds > 0, "Seconds must be above 1 but was: ", seconds);

		if (hasBar(player))
			removeBar(player);

		FakeDragon dragon = getDragon(player, message);

		dragon.name = cleanMessage(message);
		dragon.health = dragon.getMaxHealth();

		final float dragonHealthMinus = dragon.getMaxHealth() / seconds;

		cancelTimer(player);

		timers.put(player.getUniqueId(), Bukkit.getScheduler().runTaskTimer(ULibrary.getLibraryPlugin(), new Runnable() {

			@Override
			public void run() {
				FakeDragon drag = getDragon(player, "");
				drag.health -= dragonHealthMinus;

				if (drag.health <= 1) {
					removeBar(player);
					cancelTimer(player);
				} else {
					sendDragon(drag, player);
				}
			}

		}, 20L, 20L).getTaskId());

		sendDragon(dragon, player);
	}

	/**
	 * Checks whether the given player has a bar.
	 *
	 * @param player The player who should be checked.
	 *
	 * @return True, if the player has a bar, False otherwise.
	 */

	public boolean hasBar(Player player) {
		return players.get(player.getUniqueId()) != null;
	}

	/**
	 * Removes the bar from the given player.<br>
	 * If the player has no bar, this method does nothing.
	 *
	 * @param player The player whose bar should be removed.
	 */

	public void removeBar(Player player) {
		if (!hasBar(player))
			return;

		FakeDragon dragon = getDragon(player, "");

		if (dragon instanceof v1_9P) {
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, (PacketContainer) dragon.getDestroyPacket());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			Util.sendPacket(player, getDragon(player, "").getDestroyPacket());
		}

		players.remove(player.getUniqueId());

		cancelTimer(player);
	}

	/**
	 * Modifies the health of an existing bar.<br>
	 * If the player has no bar, this method does nothing.
	 *
	 * @param player	The player whose bar should be modified.
	 * @param percent The percentage of the health bar filled.<br>
	 *								This value must be between 0F and 100F (inclusive).
	 */

	public void setHealth(Player player, float percent) {
		if (!hasBar(player))
			return;

		FakeDragon dragon = getDragon(player, "");
		dragon.health = (percent / 100f) * dragon.getMaxHealth();

		cancelTimer(player);

		if (percent == 0) {
			removeBar(player);
		} else {
			sendDragon(dragon, player);
		}
	}

	/**
	 * Get the health of an existing bar.
	 *
	 * @param player The player whose bar's health should be returned.
	 *
	 * @return The current absolute health of the bar.<br>
	 * If the player has no bar, this method returns -1.
	 */

	public float getHealth(Player player) {
		if (!hasBar(player))
			return -1;

		return getDragon(player, "").health;
	}

	/**
	 * Get the message of an existing bar.
	 *
	 * @param player The player whose bar's message should be returned.
	 *
	 * @return The current message displayed to the player.<br>
	 * If the player has no bar, this method returns an empty string.
	 */

	public String getMessage(Player player) {
		if (!hasBar(player)) return "";
		return getDragon(player, "").name;
	}

	private String cleanMessage(String message) {
		if (message.length() > 64)
			message = message.substring(0, 63);
		return message;
	}

	private void cancelTimer(Player player) {
		Integer timerID = timers.remove(player.getUniqueId());

		if (timerID != null) {
			Bukkit.getScheduler().cancelTask(timerID);
		}
	}

	private void sendDragon(FakeDragon dragon, Player player) {
		if (dragon instanceof v1_9P) {
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, (PacketContainer) dragon.getTeleportPacket(player.getLocation()));
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			Util.sendPacket(player, dragon.getMetaPacket(dragon.getWatcher()));
			Util.sendPacket(player, dragon.getTeleportPacket(getDragonLocation(player, player.getLocation())));
		}
	}

	private FakeDragon getDragon(Player player, String message) {
		if (hasBar(player)) {
			return players.get(player.getUniqueId());
		} else
			return addDragon(player, cleanMessage(message));
	}

	private FakeDragon addDragon(Player player, String message) {
		FakeDragon dragon = Util.newDragon(message, getDragonLocation(player, player.getLocation()));

		if (dragon instanceof v1_9P) {
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, (PacketContainer) dragon.getSpawnPacket());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			Util.sendPacket(player, dragon.getSpawnPacket());
		}

		players.put(player.getUniqueId(), dragon);

		return dragon;
	}

	private FakeDragon addDragon(Player player, Location loc, String message) {
		FakeDragon dragon = Util.newDragon(message, getDragonLocation(player, loc));

		if (dragon instanceof v1_9P){
			try {
				ProtocolLibrary.getProtocolManager().sendServerPacket(player, (PacketContainer) dragon.getSpawnPacket());
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			Util.sendPacket(player, dragon.getSpawnPacket());
		}

		players.put(player.getUniqueId(), dragon);

		return dragon;
	}

	public Location getDragonLocation(Player player, Location loc) {
		if((!useSpigotHack && ULibrary.getNMSVersion().isBefore(NMSVersion.v1_8_R1)) ||
				(useSpigotHack && ReflectionUtil.getProtocolVersion(player) < 6) ||
				(useProtocolSupport && 		ProtocolSupportAPI.getProtocolVersion(player).isBefore(ProtocolVersion.MINECRAFT_1_8))) {
			loc.add(0, -300, 0);
		} else {
			if (Util.isBelowGround) {
				loc.subtract(0, 300, 0);
				return loc;
			}

			float pitch = loc.getPitch();

			if (pitch >= 55) {
				loc.add(0, -300, 0);
			} else if (pitch <= -55) {
				loc.add(0, 300, 0);
			} else {
				loc = loc.getBlock().getRelative(getDirection(loc), Bukkit.getViewDistance() * 16).getLocation();
				loc.add(0,-10, 0);
			}
		}

		return loc;
	}

	private BlockFace getDirection(Location loc) {
		float dir = Math.round(loc.getYaw() / 90);
		if (dir == -4 || dir == 0 || dir == 4)
			return BlockFace.SOUTH;
		if (dir == -1 || dir == 3)
			return BlockFace.EAST;
		if (dir == -2 || dir == 2)
			return BlockFace.NORTH;
		if (dir == -3 || dir == 1)
			return BlockFace.WEST;
		return null;
	}

	public void clearBar() {
		for (Player player : ULibrary.getOnlinePlayers()) {
			quit(player);
		}

		players.clear();

		for (int timerID : timers.values()) {
			Bukkit.getScheduler().cancelTask(timerID);
		}

		timers.clear();
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void PlayerLoggout(PlayerQuitEvent event) {
		quit(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerKick(PlayerKickEvent event) {
		quit(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerTeleport(final PlayerTeleportEvent event) {
		handleTeleport(event.getPlayer(), event.getTo().clone());
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerTeleport(final PlayerRespawnEvent event) {
		handleTeleport(event.getPlayer(), event.getRespawnLocation().clone());
	}

	private void handleTeleport(final Player player, final Location loc) {

		if (!hasBar(player))
			return;

		final FakeDragon oldDragon = getDragon(player, "");

		Bukkit.getScheduler().runTaskLater(ULibrary.getLibraryPlugin(), new Runnable() {

			@Override
			public void run() {
				// Check if the player still has a dragon after the two ticks! ;)
				if (!hasBar(player))
					return;

				float health = oldDragon.health;
				String message = oldDragon.name;

				Util.sendPacket(player, getDragon(player, "").getDestroyPacket());

				players.remove(player.getUniqueId());

				FakeDragon dragon = addDragon(player, loc, message);
				dragon.health = health;

				sendDragon(dragon, player);
			}

		}, 2L);
	}

	private void quit(Player player) {
		removeBar(player);
	}
}
