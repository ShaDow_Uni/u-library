package shadowuni.plugin.library.api.command.a;

import java.io.FileInputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.RequiredArgsConstructor;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.UPlugin;
import shadowuni.plugin.library.api.util.PluginUtil;
import shadowuni.plugin.library.api.util.ReflectionUtil;
import shadowuni.plugin.library.api.util.StringUtil;

@RequiredArgsConstructor
public class CommandManager implements CommandExecutor, TabCompleter {
	
	private final UPlugin plugin;
	
	private final HashMap<String, CommandListener> mainCommandListeners = new HashMap<>();
	private final HashMap<String, Method> mainCommandMethods = new HashMap<>();
	private final TreeMap<String, MainCommand> mainCommands = new TreeMap<>();
	
	private final HashMap<String, CommandListener> subCommandListeners = new HashMap<>();
	private final HashMap<String, Method> subCommandMethods = new HashMap<>();
	private final TreeMap<String, SubCommand> subCommands = new TreeMap<>();
	
	private boolean isCommandHandler(Method method) {
		return method.getAnnotation(CommandHandler.class) != null;
	}
	
	public boolean isSubCommandHandler(Method method) {
		return method.getAnnotation(SubCommandHandler.class) != null;
	}
	
	public Command getCommand(String command) {
		if(command.split(" ").length < 2) return getMainCommand(command);
		
		Command cmd = getSubCommand(command);
		
		return cmd == null ? getMainCommand(command) : cmd;
	}
	
	public MainCommand getMainCommand(String command) {
		String cmd = command.split(" ")[0];
		
		for(MainCommand mc : mainCommands.values()) {
			if(mc.getName().equalsIgnoreCase(cmd)) return mc;
			
			for(String as : mc.getAliases()) {
				if(cmd.equalsIgnoreCase(as)) return mc;
			}
		}
		
		return null;
	}
	
	public SubCommand getSubCommand(String command) {
		String[] cs = command.toLowerCase().split(" ");
		
		if(cs.length < 2) return null;
		
		String temp = getMainCommand(cs[0]).getName().toLowerCase();
		
		for(int i = 1; i < cs.length; i++) {
			a: for(SubCommand sc : subCommands.values()) {
				if(sc.getCommand().split(" ").length != i + 1) continue;
				else if(sc.getName().equalsIgnoreCase(cs[i])) {
					temp += " " + sc.getName().toLowerCase(); break a;
				}
				
				for(String as : sc.getAliases()) {
					if(!as.equalsIgnoreCase(cs[i])) continue;
					temp += " " + sc.getName().toLowerCase(); break a;
				}
			}
		}
		
		return subCommands.get(temp);
	}
	
	public List<SubCommand> getSubCommands(String command, int args) {
		List<SubCommand> cmds = new ArrayList<>();
		
		Command cmd = getCommand(command);
		
		String temp = cmd instanceof MainCommand ? cmd.getName().toLowerCase() : ((SubCommand) cmd).getCommand().toLowerCase();
		String[] cs = temp.toLowerCase().split(" ");
		
		for(SubCommand sc : subCommands.values()) {
			int sl = sc.getCommand().split(" ").length;
			if(sl <= cs.length || sl > cs.length + args || !sc.getCommand().toLowerCase().startsWith(temp)) continue;
			cmds.add(sc);
		}
		
		return cmds;
	}
	
	public List<SubCommand> getSubCommands(String command) {
		List<SubCommand> cmds = new ArrayList<>();
		
		Command cmd = getCommand(command);
		
		String temp = cmd instanceof MainCommand ? cmd.getName().toLowerCase() : ((SubCommand) cmd).getCommand().toLowerCase();
		String[] cs = temp.toLowerCase().split(" ");
		
		for(SubCommand sc : subCommands.values()) {
			int sl = sc.getCommand().split(" ").length;
			if(sl <= cs.length || !sc.getCommand().toLowerCase().startsWith(temp)) continue;
			cmds.add(sc);
		}
		
		return cmds;
	}
	
	public int registerCommands(JavaPlugin plugin) {
		int i = 0;
		try {
			ZipInputStream jarStream = new ZipInputStream(new FileInputStream(PluginUtil.getFile(plugin)));
			ZipEntry item = null;
			while((item = jarStream.getNextEntry()) != null) {
				if(item.isDirectory() || !item.getName().endsWith(".class")) continue;
				String className = item.getName().replaceAll("/", ".");
				Class<?> c = Class.forName(className.substring(0, className.length() - 6));
				try {
					CommandListener listener = (CommandListener) c.newInstance();
					if(listener instanceof UnRegisterableCommand) continue;
					registerCommands(plugin, listener);
					i++;
				} catch (Exception ex) { }
			}
			jarStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
	public int registerCommands(JavaPlugin plugin, String pack) {
		int i = 0;
		try {
			ZipInputStream jarStream = new ZipInputStream(new FileInputStream(PluginUtil.getFile(plugin)));
			ZipEntry item = null;
			while((item = jarStream.getNextEntry()) != null) {
				if(item.isDirectory() || !item.getName().endsWith(".class")) continue;
				String className = item.getName().replaceAll("/", ".").substring(0, item.getName().length() - 6);
				if(!className.substring(0, className.lastIndexOf(".")).equals(pack)) continue;
				Class<?> c = Class.forName(className);
				try {
					CommandListener listener = (CommandListener) c.newInstance();
					registerCommands(plugin, listener);
					i++;
				} catch (Exception ex) { }
			}
			jarStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
	public void registerCommands(JavaPlugin plugin, CommandListener commandListener) {
		for(Method method : commandListener.getClass().getMethods()) {
			Class<?>[] parameters = method.getParameterTypes();
			
			if(!(parameters.length == 2 || parameters.length == 3)
					|| !CommandSender.class.isAssignableFrom(parameters[0]) || !String[].class.isAssignableFrom(parameters[1])
					|| (parameters.length == 3 && !Command.class.isAssignableFrom(parameters[2]))) continue;
			
			if(isCommandHandler(method)) {
				CommandHandler anno = method.getAnnotation(CommandHandler.class);
				
				PluginCommand pc = ReflectionUtil.getCommand(anno.name(), plugin);
				
				if(pc == null) continue;
				
				pc.setExecutor(this);
				
				MainCommand mc = new MainCommand(this.plugin, anno.name());
				
				if(anno.aliases().length > 1 && !anno.aliases()[0].isEmpty()) {
					pc.setAliases(Arrays.asList(anno.aliases()));
					mc.setAliases(pc.getAliases());
				}
				
				if(!anno.additional().isEmpty()) {
					mc.setAdditional(anno.additional());
				}
				
				if(!anno.description().isEmpty()) {
					pc.setDescription(anno.description());
				}
				
				if(!anno.usage().isEmpty()) {
					pc.setUsage(anno.usage());
					mc.setUsage(anno.usage());
				}
				
				mc.setMinArgs(anno.minArgs());
				
				if(!anno.permission().isEmpty()) {
					pc.setPermission(anno.permission());
					mc.setPermission(anno.permission());
				}
				
				if(!anno.noPermissionMessage().isEmpty()) {
					pc.setPermissionMessage(anno.noPermissionMessage());
					mc.setNoPermissionMessage(anno.noPermissionMessage());
				}
				
				mc.setPlayerOnlyMessage(anno.playerOnlyMessage());
				mc.setConsoleOnlyMessage(anno.consoleOnlyMessage());
				
				ReflectionUtil.getCommandMap().register(plugin.getName(), pc);
				
				mainCommands.put(anno.name().toLowerCase(), mc);
				mainCommandListeners.put(anno.name().toLowerCase(), commandListener);
				mainCommandMethods.put(anno.name().toLowerCase(), method);
			} else if(isSubCommandHandler(method)) {
				SubCommandHandler anno = method.getAnnotation(SubCommandHandler.class);
				
				SubCommand sc = new SubCommand(this.plugin, anno.name(), anno.parent());
				if(!anno.aliases().equals(new String[] { "" })) {
					sc.setAliases(Arrays.asList(anno.aliases()));
				}
				
				if(!anno.additional().isEmpty()) {
					sc.setAdditional(anno.additional());
				}
				
				if(!anno.usage().isEmpty()) {
					sc.setUsage(anno.usage());
				}
				
				sc.setMinArgs(anno.minArgs());
				
				if(!anno.permission().isEmpty()) {
					sc.setPermission(anno.permission());
				}
				
				if(!anno.noPermissionMessage().isEmpty()) {
					sc.setNoPermissionMessage(anno.noPermissionMessage());
				}
				
				sc.setPlayerOnlyMessage(anno.playerOnlyMessage());
				sc.setConsoleOnlyMessage(anno.consoleOnlyMessage());
				
				subCommands.put(sc.getCommand().toLowerCase(), sc);
				subCommandListeners.put(sc.getCommand().toLowerCase(), commandListener);
				subCommandMethods.put(sc.getCommand().toLowerCase(), method);
			}
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
		String cs = label + (args.length < 1 ? "" : " " + StringUtil.connectString(args, " "));
		
		Command c = getCommand(cs);
		
		if(c == null) return false;
		
		String tc = c instanceof MainCommand ? c.getName().toLowerCase() : ((SubCommand) c).getCommand().toLowerCase();
		
		Object listener = c instanceof MainCommand ? mainCommandListeners.get(tc) : subCommandListeners.get(tc);
		Method method = c instanceof MainCommand ? mainCommandMethods.get(tc) : subCommandMethods.get(tc);
		
		if(method.getParameterTypes()[0].equals(Player.class) && !(sender instanceof Player)) {
			ULibrary.wmsg(sender, plugin.getPluginPackage(), c.getPlayerOnlyMessage());
			return true;
		} else if(method.getParameterTypes()[0].equals(ConsoleCommandSender.class) && !(sender instanceof ConsoleCommandSender)) {
			ULibrary.wmsg(sender, plugin.getPluginPackage(), c.getConsoleOnlyMessage());
			return true;
		} else if(c.getPermission() != null && !sender.hasPermission(c.getPermission())) {
			ULibrary.wmsg(sender, plugin.getPluginPackage(), c.getNoPermissionMessage());
			return true;
		}
		
		String[] nargs = args;
		if(c instanceof SubCommand) {
			int nl = args.length - (tc.split(" ").length - 1);
			
			nargs = new String[nl];
			
			for(int i = 0; i < nargs.length; i++) {
				nargs[i] = args[args.length - nl + i];
			}
		}
		
		if(c.getMinArgs() > nargs.length) {
			if(c.getUsage() == null) return true;
			
			String entered = label;
			
			if(c instanceof SubCommand) {
				int l = tc.split(" ").length - 1;
				
				for(int i = 0; i < l; i++) {
					entered += " " + args[i];
				}
			}
			
			c.sendUsage(sender, entered, true);
			return true;
		}
		
		try {
			if(method.getParameterCount() == 2) {
				method.invoke(listener, sender, nargs);
			} else {
				method.invoke(listener, sender, nargs, c);
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
		String cs = label + (args.length < 1 ? "" : " " + StringUtil.connectString(args, " "));
		
		Command c = getCommand(cs);
		
		if(c == null) return null;
		
		String tc = c instanceof MainCommand ? c.getName().toLowerCase() : ((SubCommand) c).getCommand().toLowerCase();
		
		List<String> commands = new ArrayList<>();
		List<SubCommand> subCommands = getSubCommands(tc, 1);
		
		if(subCommands.size() < 1) return null;
		
		for(SubCommand sc : subCommands) {
			commands.add(sc.getName());
			for(String as : sc.getAliases()) {
				commands.add(as);
			}
		}
		
		Collections.sort(commands);
		
		return commands;
	}
	
}