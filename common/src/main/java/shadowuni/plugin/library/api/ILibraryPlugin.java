package shadowuni.plugin.library.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import shadowuni.plugin.library.api.util.PluginUtil;
import shadowuni.plugin.library.listener.ChannelMessageListener;
import shadowuni.plugin.library.listener.GUIListener;

public class ILibraryPlugin extends UPlugin {
	
	@Getter
	private static ILibraryPlugin instance;
	
	@Getter
	private static ULibrary api;
	
	public void onUEnable() {
		instance = this;
		api.init();
		
		registerListeners(GUIListener.class.getPackage().getName());
		registerChannel();
		
		pluginCheck();
		
		if(api.getSQLManager().isUse()) {
			api.getSQLManager().connect(this);
		}
	}
	
	public void onUDisable() {
		api.getBar().clearBar();
		
		for(String name : api.getGUIManager().getPlayerGUIs().keySet()) {
			Player p = Bukkit.getPlayer(name);
			if(p == null) return;
			p.closeInventory();
		}
		
		if(api.getSQLManager().isUse()) {
			api.getSQLManager().close();
		}
	}
	
	public void pluginCheck() {
		api.useProtocolSupport = PluginUtil.existsPlugin("ProtocolSupport");
		lib.getBar().setUseProtocolSupport(api.useProtocolSupport);
	}
	
	public void registerChannel() {
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "U-BLibrary", new ChannelMessageListener());
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "U-Library");
	}
	
}