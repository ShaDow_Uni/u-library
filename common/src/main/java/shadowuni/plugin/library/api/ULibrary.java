package shadowuni.plugin.library.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.library.api.category.NMSVersion;
import shadowuni.plugin.library.api.gui.GUIManager;
import shadowuni.plugin.library.api.lib.VaultHandler;
import shadowuni.plugin.library.api.lib.barapi.BarAPI;
import shadowuni.plugin.library.api.permission.PermissionManager;
import shadowuni.plugin.library.api.sql.SQLManager;
import shadowuni.plugin.library.api.util.ReflectionUtil;

public class ULibrary {
	
	@Getter
	protected static ULibrary instance;
	
	@Getter
	protected static UILibrary UILibrary;
	
	@Setter
	@Getter
	protected static UPlugin libraryPlugin;
	
	@Getter
	private static final String libPrefix = "§e[ U-Library ] §f";
	
	@Getter
	protected static boolean useProtocolSupport;
	
	@Getter
	private static NMSVersion NMSVersion;
	
	@Getter
	private static HashMap<String, String> pluginPrefixes = new HashMap<>(); // <Package, PluginPrefix>
	@Getter
	private static HashMap<String, ChatColor> pluginColors = new HashMap<>(); // <Package, Color>
	
	@Getter
	private static HashMap<String, String> pluginLogFormat = new HashMap<>(); // <Package, Format>
	@Getter
	private static HashMap<String, String> pluginWarningLogFormat = new HashMap<>(); // <Package, Format>
	@Getter
	private static HashMap<String, String> pluginMessageFormat = new HashMap<>(); // <Package, Format>
	@Getter
	private static HashMap<String, String> pluginWarningMessageFormat = new HashMap<>(); // <Package, Format>
	
	@Getter
	private static BarAPI bar;
	@Getter
	private static VaultHandler vault;
	@Getter
	private static GUIManager GUIManager;
	@Getter
	private static PermissionManager permissionManager;
	@Getter
	private static SQLManager SQLManager;
	
	public ULibrary() {
		instance = this;
	}
	
	public static void init() {
		bar = new BarAPI();
		vault = new VaultHandler();
		GUIManager = new GUIManager();
		permissionManager = new PermissionManager();
		SQLManager = new SQLManager();
		
		if(Bukkit.getPluginManager().getPlugin("Vault") != null) {
			vault.setupChat();
			vault.setupPermission();
			vault.setupEconomy();
		}
		
		String pg = Bukkit.getServer().getClass().getPackage().getName();
		NMSVersion = shadowuni.plugin.library.api.category.NMSVersion.getByName(pg.substring(pg.lastIndexOf(".") + 1));
	}
	
	public static String getLastClassName() {
		StackTraceElement[] ste = new Throwable().getStackTrace();
		for(int i = 0; i < ste.length; i++) {
			if(!ste[i].getClassName().startsWith(ILibraryPlugin.class.getPackage().getName().substring(0, ILibraryPlugin.class.getPackage().getName().lastIndexOf(".")))) return ste[i].getClassName();
		}
		return null;
	}
	
	public static void log(Object message) {
		log(getLastClassName(), message);
	}
	
	public static void log(String pack, Object message) {
		nlog(String.format(getLogFormat(pack), message.toString()));
	}
	
	public static void wlog(Object message) {
		wlog(getLastClassName(), message);
	}
	
	public static void wlog(String pack, Object message) {
		nlog(String.format(getWarningLogFormat(pack), message.toString()));
	}
	
	public static void nlog(Object message) {
		Bukkit.getConsoleSender().sendMessage(message.toString());
	}
	
	public static void msg(CommandSender sender, Object message) {
		msg(sender, getLastClassName(), message);
	}
	
	public static void msg(CommandSender sender, String pack, Object message) {
		nmsg(sender, String.format(getMessageFormat(pack), message.toString()));
	}
	
	public static void wmsg(CommandSender sender, Object message) {
		wmsg(sender, getLastClassName(), message);
	}
	
	public static void wmsg(CommandSender sender, String pack, Object message) {
		nmsg(sender, String.format(getWarningMessageFormat(pack), message.toString()));
	}
	
	public static void nmsg(CommandSender sender, Object message) {
		sender.sendMessage(message.toString());
	}
	
	public static void broadcast(Object message) {
		nbroadcast(getPrefix(getLastClassName()) + message.toString());
	}
	
	public static void nbroadcast(Object message) {
		Bukkit.broadcastMessage(message.toString());
	}
	
	public static void registerPrefix(String startPackage, String prefix) {
		pluginPrefixes.put(startPackage, prefix);
	}
	
	public static void registerColor(String startPackage, ChatColor color) {
		pluginColors.put(startPackage, color);
	}
	
	public static void registerLogFormat(String startPackage, String format) {
		pluginLogFormat.put(startPackage, format);
	}
	
	public static void registerWarningLogFormat(String startPackage, String format) {
		pluginWarningLogFormat.put(startPackage, format);
	}
	
	public static void registerMessageFormat(String startPackage, String format) {
		pluginMessageFormat.put(startPackage, format);
	}
	
	public static void registerWarningMessageFormat(String startPackage, String format) {
		pluginWarningMessageFormat.put(startPackage, format);
	}
	
	public static String getPrefix(String classPackage) {
		if(classPackage == null) return libPrefix;
		
		for(String pack : pluginPrefixes.keySet()) {
			if(classPackage.equals(pack) || (classPackage.startsWith(pack) && classPackage.length() > pack.length() && classPackage.substring(pack.length(), pack.length() + 1).equals("."))) return pluginPrefixes.get(pack);
		}
		
		return libPrefix;
	}
	
	public static ChatColor getPluginColor(String classPackage) {
		if(classPackage == null) return ChatColor.WHITE;
		
		for(String pack : pluginColors.keySet()) {
			if(classPackage.startsWith(pack)) return pluginColors.get(pack);
		}
		
		return ChatColor.WHITE;
	}
	
	public static String getLogFormat(String classPackage) {
		String defaultFormat = getPrefix(classPackage) + "%s";
		
		if(classPackage == null) return defaultFormat;
		for(String pack : pluginLogFormat.keySet()) {
			if(classPackage.equals(pack) || (classPackage.startsWith(pack) && classPackage.length() > pack.length() && classPackage.substring(pack.length(), pack.length() + 1).equals("."))) return pluginLogFormat.get(pack);
		}
		
		return defaultFormat;
	}
	
	public static String getWarningLogFormat(String classPackage) {
		String defaultFormat = getPrefix(classPackage) + "%s";
		
		if(classPackage == null) return defaultFormat;
		for(String pack : pluginWarningLogFormat.keySet()) {
			if(classPackage.equals(pack) || (classPackage.startsWith(pack) && classPackage.length() > pack.length() && classPackage.substring(pack.length(), pack.length() + 1).equals("."))) return pluginWarningLogFormat.get(pack);
		}
		
		return defaultFormat;
	}
	
	public static String getMessageFormat(String classPackage) {
		String defaultFormat = getPrefix(classPackage) + "%s";
		
		if(classPackage == null) return defaultFormat;
		for(String pack : pluginMessageFormat.keySet()) {
			if(classPackage.equals(pack) || (classPackage.startsWith(pack) && classPackage.length() > pack.length() && classPackage.substring(pack.length(), pack.length() + 1).equals("."))) return pluginMessageFormat.get(pack);
		}
		
		return defaultFormat;
	}
	
	public static String getWarningMessageFormat(String classPackage) {
		String defaultFormat = getPrefix(classPackage) + "%s";
		
		if(classPackage == null) return defaultFormat;
		for(String pack : pluginWarningMessageFormat.keySet()) {
			if(classPackage.equals(pack) || (classPackage.startsWith(pack) && classPackage.length() > pack.length() && classPackage.substring(pack.length(), pack.length() + 1).equals("."))) return pluginWarningMessageFormat.get(pack);
		}
		
		return defaultFormat;
	}
	
	public static boolean isSpigot() {
		try { return Class.forName("org.bukkit.Server.Spigot") != null; }
		catch (ClassNotFoundException e) { }
		return false;
	}
	
	public static List<Player> getOnlinePlayers() {
		List<Player> players = new ArrayList<>();
		try {
			Object result = ReflectionUtil.getMethod(Bukkit.class, "getOnlinePlayers").invoke(null, null);
			if(result instanceof Player[]) {
				players.addAll(Arrays.asList((Player[]) result));
			} else {
				players.addAll((Collection) result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return players;
	}
	
	public static void teleport(Player p, Location location) {
		if(p.getVehicle() != null) {
			p.leaveVehicle();
		}
		p.teleport(location);
	}
	
	public static Firework spawnFirework(boolean flicker, boolean trail, Type type, Color color, Color fade, int power, Location location) {
		Firework firework = location.getWorld().spawn(location, Firework.class);
		FireworkMeta meta = firework.getFireworkMeta();
		FireworkEffect fe = FireworkEffect.builder().flicker(flicker).trail(trail).with(type).withColor(color).withFade(fade).build();
		meta.setPower(power);
		meta.addEffect(fe);
		firework.setFireworkMeta(meta);
		return firework;
	}
	
	public static String getPlayerKey(String name) {
		return UILibrary.getPlayerKey(name);
	}
	
	public static String getPlayerKey(String name, boolean sql) {
		return getPlayerKey(name) != null || (!sql || !SQLManager.isUse()) ? getPlayerKey(name) : SQLManager.getPlayerKey(name);
	}
	
	public static String getPlayerKey(Player player) {
		return UILibrary.getPlayerKey(player);
	}
	
	public static String getPlayerKey(Player player, boolean sql) {
		return getPlayerKey(player) != null || (!sql || !SQLManager.isUse()) ? getPlayerKey(player) : SQLManager.getPlayerKey(player.getName());
	}
	
	public static Player getPlayerByPlayerKey(String playerKey) {
		return UILibrary.getPlayerByPlayerKey(playerKey);
	}
	
}