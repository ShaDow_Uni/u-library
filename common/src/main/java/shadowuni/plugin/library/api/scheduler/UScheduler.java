package shadowuni.plugin.library.api.scheduler;

import java.util.Date;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import lombok.Getter;

public abstract class UScheduler {
	
	private final Plugin plugin;
	
	@Getter
	private UBukkitRunnable task;
	
	public UScheduler(Plugin plugin) {
		this.plugin = plugin;
		
		init();
	}
	
	public void init() {
		task = new UBukkitRunnable(plugin) {
			@Override
			public void run() {
				urun();
			}
		};
	}
	
	public synchronized BukkitTask runTask() throws IllegalArgumentException, IllegalStateException {
		return task.runTask();
	}

	public synchronized BukkitTask runTaskAsynchronously()
			throws IllegalArgumentException, IllegalStateException {
		return task.runTaskAsynchronously();
	}

	public synchronized BukkitTask runTaskLater(long delay)
			throws IllegalArgumentException, IllegalStateException {
		return task.runTaskLater(delay);
	}

	public synchronized BukkitTask runTaskLaterAsynchronously(long delay)
			throws IllegalArgumentException, IllegalStateException {
		return task.runTaskLaterAsynchronously(delay);
	}

	public synchronized BukkitTask runTaskTimer(long delay, long period)
			throws IllegalArgumentException, IllegalStateException {
		return task.runTaskTimer(delay, period);
	}
	
	public synchronized BukkitTask runTaskTimerAsynchronously(long delay, long period)
			throws IllegalArgumentException, IllegalStateException {
		return task.runTaskTimerAsynchronously(delay, period);
	}
	
	public BukkitTask schedule(Date time) {
		return task.schedule(time);
	}
	
	public BukkitTask schedule(Date firstTime, long period) {
		return task.schedule(firstTime, period);
	}
	
	public BukkitTask scheduleAsynchronously(Date time) {
		return task.scheduleAsynchronously(time);
	}
	
	public BukkitTask scheduleAsynchronously(Date firstTime, long period) {
		return task.scheduleAsynchronously(firstTime, period);
	}
	
	public void stop() {
		if(task.getTaskId() == -1) return;
		task.cancel();
		
		init();
	}
	
	public int getTaskId() {
		return task.getTaskId();
	}
	
	public abstract void urun();
	
}