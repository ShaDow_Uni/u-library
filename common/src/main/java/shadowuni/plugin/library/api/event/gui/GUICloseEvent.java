package shadowuni.plugin.library.api.event.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;

import lombok.Getter;
import shadowuni.plugin.library.api.ULibrary;
import shadowuni.plugin.library.api.event.UEvent;
import shadowuni.plugin.library.api.gui.GUI;

public class GUICloseEvent extends UEvent {
	
	@Getter
	private final Player player;
	
	@Getter
	private final InventoryCloseEvent inventoryCloseEvent;
	
	@Getter
	private GUI GUI;
	
	public GUICloseEvent(InventoryCloseEvent inventoryCloseEvent) {
		this.inventoryCloseEvent = inventoryCloseEvent;
		player = (Player) inventoryCloseEvent.getPlayer();
		GUI = ULibrary.getGUIManager().getPlayerGUI(player);
	}

}